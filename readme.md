# Intro

This project supports working with the clinicaltrials.gov database in MySQL by loading the pipe-delimited data found at
ctti-clinicaltrials.org . The release we're working with is AACT201509_pipe_delimited_txt.zip (link below). Credit
is given to https://github.com/jasonost for his file
https://github.com/jasonost/clinicaltrials/blob/master/database/create_tables_crawler.sql (a very helpful starting
point because AACT_Oracle_DDL.sql was not included in that distribution).


# Files

* create_database_and_tables.sql: creates the 'clinical_trials_gov' database and then loads create_tables_crawler.sql
* create_tables_crawler.sql: DDL statements to create the tables
* create_tables_crawler-ORIG.sql: original file as downloaded from above
* load_tables.sql: loads the text files into the tables
* select_counts.sql: utility that prints the number of records in each table, which is helpful in comparing to expected


# Todo

* Dose, Frequency, and Duration - requires NLP on intervention.description
* Counts are very close, but off by a few records. This needs to be debugged.


# links

https://clinicaltrials.gov/ct2/resources/download#DownloadOracle
http://www.ctti-clinicaltrials.org/what-we-do/analysis-dissemination/state-clinical-trials/aact-database
http://library.dcri.duke.edu/dtmi/ctti/2015_Sept_Annual/AACT201509_pipe_delimited_txt.zip
