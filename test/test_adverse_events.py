import unittest

from ct_db_extract.extract_adverse_events import group_adverse_events


class AdverseEventsTestCase(unittest.TestCase):
    """
    """

    def test_group_adverse_events(self):
        adverse_events = [('Cardiac disorders', 'Myocardial infarction', 'Imatinib 400 mg QD', '1', '280'),
                          ('Cardiac disorders', 'Myocardial infarction', 'Nilotinib 300 mg BID', '2', '279'),
                          ('Cardiac disorders', 'Myocardial infarction', 'Nilotinib 400 mg BID', '2', '277'),
                          ('Cardiac disorders', 'Palpitations', 'Imatinib 400 mg QD', '1', '280'),
                          ('Cardiac disorders', 'Palpitations', 'Nilotinib 300 mg BID', '1', '279'),
                          ('Cardiac disorders', 'Palpitations', 'Nilotinib 400 mg BID', '0', '277'),
                          ]
        exp_grouped_events = [['Cardiac disorders', 'Myocardial infarction', 1, 280, 2, 279, 2, 277],
                              ['Cardiac disorders', 'Palpitations', 1, 280, 1, 279, 0, 277],
                              ]
        act_grouped_events = group_adverse_events(adverse_events)
        self.assertEqual(exp_grouped_events, act_grouped_events)
