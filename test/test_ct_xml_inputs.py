import unittest

from ct_xml_extract import ct_xml_utils as ct


class CtXMLUtilsInputsTestCase(unittest.TestCase):
    """
    """

    def setUp(self):
        nct_id = 'NCT00440193'
        self.nct_dict = ct.dict_for_nct_id('.', nct_id)

    def test_study_type_and_condition(self):
        self.assertEqual('Interventional', ct.get_study_type(self.nct_dict))
        self.assertEqual('Venous Thrombosis', ct.get_study_condition(self.nct_dict))

    def test_study_design(self):
        exp_design = {
            'Allocation': 'Randomized',
            'Endpoint Classification': 'Safety/Efficacy Study',
            'Intervention Model': 'Parallel Assignment',
            'Masking': 'Open Label',
            'Primary Purpose': 'Treatment',
        }
        self.assertEqual(exp_design, ct.get_study_design(self.nct_dict))

        exp_design = {
            'Allocation': 'Non-Randomized',
            'Endpoint Classification': 'Safety Study',
            'Intervention Model': 'Single Group Assignment',
            'Masking': 'Open Label',
        }
        other_nct_dict = ct.dict_for_nct_id('.', 'NCT00824369')
        self.assertEqual(exp_design, ct.get_study_design(other_nct_dict))

        # test inputs where commas are in pairs
        study_design_str = 'Allocation: Randomized, ' \
                           'Endpoint Classification: Safety/Efficacy Study, ' \
                           'Intervention Model: Parallel Assignment, ' \
                           'Masking: Double Blind (Subject, Investigator), ' \
                           'Primary Purpose: Treatment'
        exp_split = {'Allocation': 'Randomized',
                     'Endpoint Classification': 'Safety/Efficacy Study',
                     'Intervention Model': 'Parallel Assignment',
                     'Masking': 'Double Blind (Subject, Investigator)',
                     'Primary Purpose': 'Treatment',}
        self.assertEqual(exp_split, ct.split_study_design_str(study_design_str))

    def test_baseline_groups(self):
        exp_bl_groups = [('B1', 'Rivaroxaban (Xarelto, BAY59-7939)'),
                         ('B2', 'Enoxaparin/VKA'),
                         ('B3', 'Total')]
        self.assertEqual(exp_bl_groups, ct.get_baseline_groups(self.nct_dict))

        other_nct_dict = ct.dict_for_nct_id('.', 'NCT00824369')
        exp_bl_groups = [('B1', 'Participants From A5271015 - Lersivirine 500 mg Arm'),
                         ('B2', 'Participants From A5271015 - Lersivirine 750 mg Arm'),
                         ('B3', 'Participants From A5271015 - Efavirenz 600 mg Arm'),
                         ('B4', 'Participants From A5271022 - Lersivirine 750 mg Arm'),
                         ('B5', 'Participants From A5271022 - Lersivirine 1000 mg Arm'),
                         ('B6', 'Participants From A5271022 - Etravirine 200 mg Arm'),
                         ('B7', 'Total')]
        self.assertEqual(exp_bl_groups, ct.get_baseline_groups(other_nct_dict))

    def test_outcome_groups(self):
        exp_oc_groups = [('O1', 'Rivaroxaban (Xarelto, BAY59-7939)'),
                         ('O2', 'Enoxaparin/VKA')]
        self.assertEqual(exp_oc_groups, ct.get_outcome_groups(self.nct_dict))

    def test_partic_flow_groups(self):
        exp_pf_groups = [('P1', 'Rivaroxaban (Xarelto, BAY59-7939)',
                          'Participants were to receive rivaroxaban 15 mg oral tablet twice daily for 3 weeks, followed by 20 mg once daily'),
                         ('P2', 'Enoxaparin/VKA',
                          'Participants were to receive 1.0 mg/kg enoxaparin twice daily (subcutaneous) for at least 5 days, plus vitamin K antagonist (VKA) (oral) at individually titrated doses to achieve a target international normalized ratio (INR) of 2.5 (range: 2.0 to 3.0)')]
        self.assertEqual(exp_pf_groups, ct.get_participant_flow_groups(self.nct_dict))

    def test_reported_event_groups(self):
        exp_re_groups = [('E1', 'Rivaroxaban (Xarelto, BAY59-7939)'),
                         ('E2', 'Enoxaparin/VKA')]
        self.assertEqual(exp_re_groups, ct.get_reported_event_groups(self.nct_dict))

    def test_arm_periods(self):
        # 3) Participant Flow > Periods:
        exp_periods = [(0, 'Treatment Period'), (1, 'Observational Period')]
        act_periods = ct.get_periods(self.nct_dict)
        self.assertEqual(exp_periods, act_periods)

        # test other nct_id
        other_nct_dict = ct.dict_for_nct_id('.', 'NCT00824369')
        exp_periods = [(0, 'Overall Study')]
        act_periods = ct.get_periods(other_nct_dict)
        self.assertEqual(exp_periods, act_periods)

        # 4a) Participant Flow > Period 1: Treatment Period > Started/Completed/Not Completed:
        with self.assertRaises(Exception):
            ct.get_milestones(self.nct_dict, None)

        exp_milestones = [(0, 'STARTED'), (1, 'Participants Received Treatment'), (2, 'COMPLETED'),
                          (3, 'NOT COMPLETED')]
        self.assertEqual(exp_milestones, ct.get_milestones(self.nct_dict, 0))

        # test other nct_id
        exp_milestones = [(0, 'STARTED'), (1, 'COMPLETED'), (2, 'NOT COMPLETED')]
        act_milestones = ct.get_milestones(other_nct_dict, 0)
        self.assertEqual(exp_milestones, act_milestones)

        exp_drop_withdraw_reasons = [(0, 'Adverse Event'), (1, 'Death'), (2, 'Lack of Efficacy'),
                                     (3, 'Lost to Follow-up'), (4, 'Physician Decision'), (5, 'Protocol Violation'),
                                     (6, 'Withdrawal by Subject'), (7, 'Clinical endpoint reached'),
                                     (8, 'Study terminated by sponsor'), (9, 'Protocol driven decision point'),
                                     (10, 'Switch to commercial drug'), (11, 'Technical problems'),
                                     (12, 'Participant convenience'), (13, 'Participant did not receive treatment'),
                                     (14, 'Site closed by investigator')]
        act_drop_withdraw_reasons = ct.get_drop_withdraw_reasons(self.nct_dict, 0)
        self.assertEqual(exp_drop_withdraw_reasons, act_drop_withdraw_reasons)

        # 4b) Participant Flow > Period 2: Observational Period > Started/Completed/Not Completed:
        exp_milestones = [(0, 'STARTED'), (1, 'COMPLETED'), (2, 'NOT COMPLETED')]
        act_milestones = ct.get_milestones(self.nct_dict, 1)
        self.assertEqual(exp_milestones, act_milestones)

        exp_drop_withdraw_reasons = [(0, 'Adverse Event'), (1, 'Death'), (2, 'Lost to Follow-up'),
                                     (3, 'Protocol Violation'), (4, 'Withdrawal by Subject'), (5, 'Physician Decision'),
                                     (6, 'Clinical endpoint reached'), (7, 'Participant convenience')]
        act_drop_withdraw_reasons = ct.get_drop_withdraw_reasons(self.nct_dict, 1)
        self.assertEqual(exp_drop_withdraw_reasons, act_drop_withdraw_reasons)

    def test_outcomes(self):
        # 5) Outcome Measures: four types: 'primary', 'secondary', 'other pre-specified', 'post-hoc'
        exp_outcomes = [
            (0, 'Percentage of Participants With Symptomatic Recurrent Venous Thromboembolism [VTE] (i.e. the '
                'Composite of Recurrent Deep Vein Thrombosis [DVT] or Fatal or Non-fatal Pulmonary Embolism [PE]) '
                'Until the Intended End of Study Treatment',
             'Primary'),
            (1, 'Percentage of Participants With the Composite Variable Comprising Recurrent DVT, Non-fatal PE '
                'and All Cause Mortality Until the Intended End of Study Treatment',
             'Secondary'),
            (2, 'Percentage of Participants With an Event for Net Clinical Benefit 1 Until the Intended End of '
                'Study Treatment',
             'Secondary'),
            (3, 'Percentage of Participants With Recurrent DVT Until the Intended End of Study Treatment', 'Secondary'),
            (4, 'Percentage of Participants With Clinically Relevant Bleeding, Treatment-emergent (Time Window: '
                'Until 2 Days After Last Dose)',
             'Secondary'),
            (5, 'Percentage of Participants With All Deaths',
             'Secondary'),
            (6, 'Percentage of Participants With Other Vascular Events, On-treatment (Time Window: Until 1 Day '
                'After Last Dose)',
             'Secondary'),
            (7, 'Percentage of Participants With the Individual Components of Efficacy Outcomes Until the Intended '
                'End of Study Treatment',
             'Other Pre-specified'),
            (8, 'Percentage of Participants With Symptomatic Recurrent Venous Thromboembolism [VTE] (i.e. the '
                'Composite of Recurrent Deep Vein Thrombosis [DVT] or Fatal or Non-fatal Pulmonary Embolism [PE]) '
                'During Observational Period',
             'Other Pre-specified'),
            (9, 'Percentage of Participants With the Composite Variable Comprising Recurrent DVT, Non-fatal PE and '
                'All Cause Mortality During Observational Period',
             'Other Pre-specified'),
            (10, 'Percentage of Participants With an Event for Net Clinical Benefit 1 During Observational Period',
             'Other Pre-specified'),
            (11, 'Percentage of Participants With Recurrent DVT During Observational Period',
             'Other Pre-specified'),
            (12, 'Percentage of Participants With the Individual Components of Efficacy Outcomes During '
                 'Observational Period',
             'Other Pre-specified'),
            (13, 'Percentage of Participants With an Event for Net Clinical Benefit 2 Until the Intended End of '
                 'Study Treatment',
             'Post-Hoc'),
            (14, 'Percentage of Participants With an Event for Net Clinical Benefit 2 During Observational Period',
             'Post-Hoc'), ]
        self.assertEqual(exp_outcomes, ct.get_outcomes(self.nct_dict))

    def test_outcome_measures_and_units(self):
        exp_measures = [
            (0, 'Number of Participants', 'participants'),
            (1, 'Percentage of Participants With Symptomatic Recurrent Venous Thromboembolism [VTE] (i.e. the '
                'Composite of Recurrent Deep Vein Thrombosis [DVT] or Fatal or Non-fatal Pulmonary Embolism [PE]) '
                'Until the Intended End of Study Treatment',
             'Percentage of participants')]
        self.assertEqual(exp_measures, ct.get_outcome_measures(self.nct_dict, 0))

        exp_measures = [
            (0, 'Number of Participants', 'participants'),
            (1, 'Percentage of Participants With the Individual Components of Efficacy Outcomes Until the Intended '
                'End of Study Treatment',
             'Percentage of participants')]
        self.assertEqual(exp_measures, ct.get_outcome_measures(self.nct_dict, 7))

    def test_outcome_measure_cats(self):
        with self.assertRaises(Exception):
            ct.get_outcome_measures(self.nct_dict, None)

        # 6a) Outcome Measures > 1. Primary: Percentage of Participants With Symptomatic ...
        self.assertEqual([], ct.get_outcome_measure_cats(self.nct_dict, 0, 0))
        self.assertEqual([], ct.get_outcome_measure_cats(self.nct_dict, 0, 1))

        # 6b) Outcome Measures > 8. Other Pre-specified: Percentage of Participants With the ...
        exp_outcome_cats = [(0, 'Death (PE)'),
                            (1, 'Death (PE cannot be excluded)'),
                            (2, 'Symptomatic PE and DVT'),
                            (3, 'Symptomatic recurrent PE only'),
                            (4, 'Symptomatic recurrent DVT only'),
                            (5, 'Death (bleeding)'),
                            (6, 'Death (cardiovascular)'),
                            (7, 'Death (other)'),
                            (8, 'Major bleeding')]
        self.assertEqual([], ct.get_outcome_measure_cats(self.nct_dict, 7, 0))
        self.assertEqual(exp_outcome_cats, ct.get_outcome_measure_cats(self.nct_dict, 7, 1))

    def test_outcome_measure_cats_other_nct_id(self):
        other_nct_dict = ct.dict_for_nct_id('.', 'NCT00824369')
        exp_outcome_cats = [(0, 'Number of participants with adverse events'),
                            (1, 'Number of participants with serious adverse events'),
                            (2, 'Participants discontinued due to adverse events')]
        self.assertEqual([], ct.get_outcome_measure_cats(other_nct_dict, 0, 0))
        self.assertEqual(exp_outcome_cats, ct.get_outcome_measure_cats(other_nct_dict, 0, 1))

    def test_outcome_measures_no_measure_list(self):
        nct_dict = ct.dict_for_nct_id('.', 'NCT00481247')
        self.assertEqual([], ct.get_outcome_measures(nct_dict, 1))

    def test_adverse_event_cats(self):
        # 7) Serious Adverse Events:
        # 8) Other Adverse Events:
        exp_adverse_events = {
            'serious_events': [
                (0, "Total"),
                (1, "Blood and lymphatic system disorders"),
                (2, "Cardiac disorders"),
                (3, "Congenital, familial and genetic disorders"),
                (4, "Ear and labyrinth disorders"),
                (5, "Endocrine disorders"),
                (6, "Eye disorders"),
                (7, "Gastrointestinal disorders"),
                (8, "General disorders"),
                (9, "Hepatobiliary disorders"),
                (10, "Immune system disorders"),
                (11, "Infections and infestations"),
                (12, "Injury, poisoning and procedural complications"),
                (13, "Investigations"),
                (14, "Metabolism and nutrition disorders"),
                (15, "Musculoskeletal and connective tissue disorders"),
                (16, "Neoplasms benign, malignant and unspecified (incl cysts and polyps)"),
                (17, "Nervous system disorders"),
                (18, "Pregnancy, puerperium and perinatal conditions"),
                (19, "Psychiatric disorders"),
                (20, "Renal and urinary disorders"),
                (21, "Reproductive system and breast disorders"),
                (22, "Respiratory, thoracic and mediastinal disorders"),
                (23, "Skin and subcutaneous tissue disorders"),
                (24, "Surgical and medical procedures"),
                (25, "Vascular disorders")],
            'other_events': [
                (0, "Total"),
                (1, "Infections and infestations"),
                (2, "Nervous system disorders"),
                (3, "Respiratory, thoracic and mediastinal disorders")],
        }
        act_adverse_events = ct.get_adverse_event_type_to_cats(self.nct_dict)
        self.assertEqual(exp_adverse_events, act_adverse_events)

        # test when no 'serious_events' in xml
        other_nct_dict = ct.dict_for_nct_id('.', 'NCT00824369')
        act_adverse_events = ct.get_adverse_event_type_to_cats(other_nct_dict)
        self.assertEqual(0, len(act_adverse_events['serious_events']))
        self.assertEqual(20, len(act_adverse_events['other_events']))

    def test_adverse_event_cat_subtitles(self):
        self.assertEqual([(0, 'Phimosis')], ct.get_adverse_event_cat_to_events(self.nct_dict, True, 3))
        self.assertEqual([(0, 'Ear haemorrhage'), (1, 'Vertigo'), (2, 'Sudden hearing loss')],
                         ct.get_adverse_event_cat_to_events(self.nct_dict, True, 4))

        other_nct_dict = ct.dict_for_nct_id('.', 'NCT00824369')
        self.assertEqual([(0, 'Anaemia'), (1, 'Thrombocytopenia')],
                         ct.get_adverse_event_cat_to_events(other_nct_dict, False, 1))

if __name__ == '__main__':
    unittest.main()
