import json
import unittest

from ct_xml_extract import ct_xml_csv_output as ct_csv
from ct_xml_extract import ct_xml_utils as ct


class TestCtXMLCSVOutputTestCase(unittest.TestCase):
    """
    Drives outputting data as csv files. See NCT00440193-expected-output.xlsx .
    """

    def setUp(self):
        # user inputs based on print_xml_output_data_app.py running on NCT00440193
        self.nct_id = 'NCT00440193'
        self.nct_dict = ct.dict_for_nct_id('.', self.nct_id)

    def test_overall_study_rows(self):
        overall_study_rows = ct_csv.get_overall_study_rows(self.nct_id, self.nct_dict, ['P1', 'P2'])
        self.assertEqual(4, len(overall_study_rows))

        for row in overall_study_rows:
            self.assertEqual(9, len(row))  # columns

        self.assertEqual(['Overall Study', '', '', '', '', '', '', '', ''], overall_study_rows[0])
        self.assertEqual(['', '', 'Design', '', '', '', '', 'Interventions', ''], overall_study_rows[1])
        self.assertEqual(['NCT ID', 'Study Type', 'Allocation', 'Endpoint Classification', 'Intervention Model',
                          'Primary Purpose', 'Condition', 'Group 1', 'Group 2'],
                         overall_study_rows[2])
        self.assertEqual(
            ['NCT00440193', 'Interventional', 'Randomized', 'Safety/Efficacy Study', 'Parallel Assignment', 'Treatment',
             'Venous Thrombosis', 'Rivaroxaban (Xarelto, BAY59-7939)', 'Enoxaparin/VKA'],
            overall_study_rows[3])

    def test_participant_flow_rows(self):
        act_pf_rows = ct_csv.get_participant_flow_rows(self.nct_dict, 0, [0, 2, 3], [0, 1], ['P1', 'P2'])
        self.assertEqual(4, len(act_pf_rows))

        for row in act_pf_rows:
            self.assertEqual(12, len(row))  # columns

        self.assertEqual(['Participant Flow', '', '', '', '', '', '', '', '', '', '', ''], act_pf_rows[0])
        self.assertEqual(['Dosage Regimen', '', 'STARTED', '', 'COMPLETED', '', 'NOT COMPLETED', '',
                          'Adverse Event', '', 'Death', ''],
                         act_pf_rows[1])
        self.assertEqual(['Group 1', 'Group 2', 'Group 1', 'Group 2', 'Group 1', 'Group 2', 'Group 1', 'Group 2',
                          'Group 1', 'Group 2', 'Group 1', 'Group 2'],
                         act_pf_rows[2])
        self.assertEqual(['Participants were to receive rivaroxaban 15 mg oral tablet twice daily for 3 weeks, '
                          'followed by 20 mg once daily',
                          'Participants were to receive 1.0 mg/kg enoxaparin twice daily (subcutaneous) for at least '
                          '5 days, plus vitamin K antagonist (VKA) (oral) at individually titrated doses to achieve '
                          'a target international normalized ratio (INR) of 2.5 (range: 2.0 to 3.0)',
                          1731, 1718, 1426, 1367, 305, 351, 74, 67, 19, 22],
                         act_pf_rows[3])

    def test_baseline_measure_rows(self):
        act_bl_rows = ct_csv.get_baseline_measures_rows(self.nct_dict, ['B1', 'B2'])
        self.assertEqual(4, len(act_bl_rows))

        for row in act_bl_rows:
            self.assertEqual(8, len(row))  # columns

        self.assertEqual(['Baseline Measures', '', '', '', '', '', '', ''], act_bl_rows[0])
        self.assertEqual(['Number of Participants', '', 'Age', '', 'Female', '', 'Male', ''], act_bl_rows[1])
        self.assertEqual(['Group 1', 'Group 2', 'Group 1', 'Group 2', 'Group 1', 'Group 2', 'Group 1', 'Group 2'],
                         act_bl_rows[2])
        self.assertEqual([1731, 1718, 55.8, 56.4, 738, 751, 993, 967], act_bl_rows[3])

    def test_outcome_measure_rows(self):
        oc_id_cat_ids = [(0, []), (7, [0, 1])]
        act_oc_rows = ct_csv.get_outcome_measure_rows(self.nct_dict, oc_id_cat_ids, ['O1', 'O2'])
        self.assertEqual(4, len(act_oc_rows))

        for row in act_oc_rows:
            self.assertEqual(12, len(row))  # columns

        exp_oc_rows = [['Outcome 1 (Primary)', '', '', '',
                        'Outcome 2 (Other Pre-specified)', '', '', '',
                        'Outcome 2 (Other Pre-specified)', '', '', ''],
                       ['Category 1 (%)', '', '', '',
                        'Category 1 (%)', '', '', '',
                        'Category 2 (%)', '', '', ''],
                       ['Group 1: Num', 'Group 1: Denom', 'Group 2: Num', 'Group 2: Denom',
                        'Group 1: Num', 'Group 1: Denom', 'Group 2: Num', 'Group 2: Denom',
                        'Group 1: Num', 'Group 1: Denom', 'Group 2: Num', 'Group 2: Denom'],
                       [2.1, 1731, 3.0, 1718,
                        0.06, 1731, 0, 1718,
                        0.2, 1731, 0.3, 1718]]
        self.assertEqual(exp_oc_rows, act_oc_rows)

    def test_adverse_event_rows(self):
        ae_inputs = [(True, 1, 0),
                     (True, 2, 13),
                     (False, 2, 0)]
        act_ae_rows = ct_csv.get_adverse_event_rows(self.nct_dict, ae_inputs, ['E1', 'E2'])
        self.assertEqual(4, len(act_ae_rows))

        for row in act_ae_rows:
            self.assertEqual(12, len(row))  # columns

        exp_ae_rows = [['Adverse Events', '', '', '',
                        'Adverse Events', '', '', '',
                        'Adverse Events', '', '', ''],
                       ['Outcome Measure 1', '', '', '',
                        'Outcome Measure 2', '', '', '',
                        'Outcome Measure 3', '', '', ''],
                       ['Group 1: Affected', 'Group 1: At Risk', 'Group 2: Affected', 'Group 2: At Risk',
                        'Group 1: Affected', 'Group 1: At Risk', 'Group 2: Affected', 'Group 2: At Risk',
                        'Group 1: Affected', 'Group 1: At Risk', 'Group 2: Affected', 'Group 2: At Risk'],
                       [11, 1718, 8, 1711, 0, 1718, 1, 1711, 91, 1718, 70, 1711]]

        self.assertEqual(exp_ae_rows, act_ae_rows)

    def test_all_together_now(self):
        overall_study_rows = ct_csv.get_overall_study_rows(self.nct_id, self.nct_dict, ['P1', 'P2'])
        pf_rows = ct_csv.get_participant_flow_rows(self.nct_dict, 0, [0, 2, 3], [0, 1], ['P1', 'P2'])
        bl_rows = ct_csv.get_baseline_measures_rows(self.nct_dict, ['B1', 'B2'])

        oc_id_cat_ids = [(0, []),
                               (7, [0, 1])]
        oc_rows = ct_csv.get_outcome_measure_rows(self.nct_dict, oc_id_cat_ids, ['O1', 'O2'])

        ae_inputs = [(True, 1, 0),
                     (True, 2, 13),
                     (False, 2, 0)]
        ae_rows = ct_csv.get_adverse_event_rows(self.nct_dict, ae_inputs, ['E1', 'E2'])

        act_final_rows = [overall_study_rows[0] + pf_rows[0] + bl_rows[0] + oc_rows[0] + ae_rows[0],
                          overall_study_rows[1] + pf_rows[1] + bl_rows[1] + oc_rows[1] + ae_rows[1],
                          overall_study_rows[2] + pf_rows[2] + bl_rows[2] + oc_rows[2] + ae_rows[2],
                          overall_study_rows[3] + pf_rows[3] + bl_rows[3] + oc_rows[3] + ae_rows[3],
                          ]

        # REF: to output as json or tsv:
        #
        # with open('/tmp/temp.json', 'w') as file:
        #     json.dump(final_rows, file, indent=4)
        #
        # with open('/tmp/temp.tsv', 'w') as csvfile:
        #     writer = csv.writer(csvfile, dialect=csv.excel_tab)
        #     for row in final_rows:
        #         writer.writerow(row)

        with open('NCT00440193-csv-final-rows.json') as json_file:
            exp_final_rows = json.load(json_file)
            self.assertEqual(exp_final_rows, act_final_rows)


if __name__ == '__main__':
    unittest.main()
