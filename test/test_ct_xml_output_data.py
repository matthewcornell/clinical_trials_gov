import unittest

from ct_xml_extract import ct_xml_utils as ct


class TestCtXMLSimUserInputTestCase(unittest.TestCase):
    """
    Simulates user choices (see ct_xml_extract/print_xml_input_data_app.py for what they might choose from) to test extracting
    actual data to output as a csv file.
    """

    def setUp(self):
        nct_id = 'NCT00643201'
        self.nct_dict = ct.dict_for_nct_id('.', nct_id)

    def test_participant_flow(self):
        period = 0  # Randomized, Completed 6 Months of Study
        milestone_ids_p0 = [0, 2]  # ['STARTED', 'NOT COMPLETED']
        pf_group_ids = ['P1', 'P2']
        exp_data = [[2691, 2704], [377, 413]]  # [p0_m0_P1, p0_m0_P2], [p0_m2_P1, p0_m2_P2]
        act_data = ct.get_milestone_data(self.nct_dict, period, milestone_ids_p0, pf_group_ids)
        self.assertEqual(exp_data, act_data)

        dw_reason_ids_p0 = list(range(9))  # all 9 of them: 'Death', ..., 'not specified'
        exp_drop_withdraw_data = [[20, 26], [150, 182], [49, 49], [14, 14], [20, 23], [3, 2], [13, 9], [1, 1],
                                  [107, 107]]
        act_drop_withdraw_data = ct.get_drop_withdraw_data(self.nct_dict, period, dw_reason_ids_p0, pf_group_ids)
        self.assertEqual(exp_drop_withdraw_data, act_drop_withdraw_data)

        # test with just one group -> one column
        exp_data = [[2691], [377]]
        act_data = ct.get_milestone_data(self.nct_dict, period, milestone_ids_p0, ['P1'])
        self.assertEqual(exp_data, act_data)

    def test_baseline(self):
        measure_titles = ['Number of Participants', 'Age', 'Gender']
        exp_data = {'Number of Participants': [2691, 2704],
                    'Age': [57.2, 56.7],
                    'Female': [1122, 1106],
                    'Male': [1569, 1598]}  # NB: Gender splits into two sub-categories with these titles
        # todo xx this should probably accept measure_ids instead of titles, for consistency with other methods
        act_data = ct.get_baseline_data(self.nct_dict, measure_titles, ['B1', 'B2'])
        self.assertEqual(exp_data, act_data)

    def test_outcome(self):
        # 1. (id 0) Primary: both measured values (no subcategories available)
        act_data = ct.get_outcome_measure_data(self.nct_dict, 0, 0, None, ['O1', 'O2'])
        self.assertEqual([2609, 2635], act_data)

        act_data = ct.get_outcome_measure_data(self.nct_dict, 0, 1, None, ['O1', 'O2'])
        self.assertEqual([0.0226, 0.0269], act_data)

        # 16. (id 15) Secondary: both measured values, and only two subcategories of second value
        act_data = ct.get_outcome_measure_data(self.nct_dict, 15, 0, None, ['O1', 'O2'])
        self.assertEqual([2676, 2689], act_data)

        act_data = ct.get_outcome_measure_data(self.nct_dict, 15, 1, 0, ['O1', 'O2'])
        self.assertEqual([1795, 1923], act_data)

        act_data = ct.get_outcome_measure_data(self.nct_dict, 15, 1, 1, ['O1', 'O2'])
        self.assertEqual([417, 410], act_data)

    def test_adverse_events(self):
        act_data = ct.get_adverse_event_data(self.nct_dict, True, 2, 20, ['E1', 'E2'])
        self.assertEqual([[2, 2676], [3, 2689]], act_data)

        act_data = ct.get_adverse_event_data(self.nct_dict, True, 2, 17, ['E1', 'E2'])
        self.assertEqual([[1, 2676], [5, 2689]], act_data)

        act_data = ct.get_adverse_event_data(self.nct_dict, False, 1, 0, ['E1', 'E2'])
        self.assertEqual([[73, 2676], [87, 2689]], act_data)

        # test a different nct_id
        other_nct_id = 'NCT00440193'
        other_nct_dict = ct.dict_for_nct_id('.', other_nct_id)
        act_data = ct.get_adverse_event_data(other_nct_dict, False, 2, 0, ['E1', 'E2'])
        self.assertEqual([[91, 1718], [70, 1711]], act_data)


if __name__ == '__main__':
    unittest.main()
