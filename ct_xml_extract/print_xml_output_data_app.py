import click

from ct_xml_extract import ct_xml_utils as ct


@click.command()
def print_xml_data():
    """
    An app that demonstrates features tested in test_ct_xml_output_data.py, by simulating user selections from
    test_ct_xml_inputs.py . The printed information is basically what will be exported to a csv file.
    """
    nct_repo_dir = 'test'
    nct_id = 'NCT00440193'
    nct_dict = ct.dict_for_nct_id(nct_repo_dir, nct_id)

    click.echo("==== {} ====".format(nct_id))

    # * general
    click.echo("\n* NCT ID, Study Type:")
    study_type = ct.get_study_type(nct_dict)
    click.echo("- {}".format(nct_id))
    click.echo("- {}".format(study_type))

    click.echo("\n* Design")
    design = ct.get_study_design(nct_dict)
    for k, v in design.items():
        click.echo("- {}: {}".format(k, v))

    click.echo("\n* Condition:")
    condition = ct.get_study_condition(nct_dict)
    click.echo("- {}".format(condition))

    # * Groups (Interventions)
    click.echo("\n* Groups (Interventions)")
    pf_groups = ct.get_participant_flow_groups(nct_dict)
    for group_id, group_title, group_description in pf_groups:
        click.echo("- {}".format(group_title))

    # * Participant Flow
    click.echo("\n* Participant Flow:")

    # * Participant Flow > milestones
    period_id = 0  # 'Treatment Period'
    periods = ct.get_periods(nct_dict)
    period_title = periods[period_id][1]
    click.echo("- Milestones ('{}' period only)".format(period_title))
    milestone_ids_p0 = list(range(3))  # all milestones for period 0
    pf_group_ids = ['P2', 'P1']
    milestone_data = ct.get_milestone_data(nct_dict, period_id, milestone_ids_p0, pf_group_ids)
    milestones = ct.get_milestones(nct_dict, period_id)
    for milestone_id, data_row in enumerate(milestone_data):
        milestone_title = milestones[milestone_id][1]
        click.echo("  = {}: {}".format(milestone_title, data_row))

    # * Participant Flow > NOT COMPLETED reasons (drop/withdraw)
    click.echo("- NOT COMPLETED Reasons ('{}' period only)".format(period_title))
    dw_reason_ids = [0, 1]  # ['Adverse Event', 'Death']
    drop_withdraw_data = ct.get_drop_withdraw_data(nct_dict, period_id, dw_reason_ids, pf_group_ids)
    drop_withdraw_reasons = ct.get_drop_withdraw_reasons(nct_dict, period_id)
    for dw_id, data_row in enumerate(drop_withdraw_data):
        dw_reason_title = drop_withdraw_reasons[dw_id][1]
        click.echo("  = {}: {}".format(dw_reason_title, data_row))

    click.echo("\n* Reporting Groups (Dosage Regimens): TBD")
    # act_group_dosage_regimens = ct.get_group_dosage_regimens(self.nct_dict, ??)

    # * Baseline measures
    click.echo("\n* Baseline Measures:")
    bl_measure_titles = ['Number of Participants', 'Age', 'Gender']
    bl_group_ids = ['B1', 'B2']
    baseline_data = ct.get_baseline_data(nct_dict, bl_measure_titles, bl_group_ids)
    for k, v in baseline_data.items():
        click.echo("- {}: {}".format(k, v))

    # * Outcomes
    click.echo("\n* Outcomes:")
    oc_id_meas_id_cats = [[0, 0, None], [0, 1, None], [7, 0, None], [7, 1, [0, 1]]]
    for outcome_id, meas_id, cat_ids in oc_id_meas_id_cats:
        outcomes = ct.get_outcomes(nct_dict)
        outcome_title = outcomes[outcome_id][1]
        outcome_type = outcomes[outcome_id][2]
        measures = ct.get_outcome_measures(nct_dict, outcome_id)
        measure_title = measures[meas_id][1]
        measure_units = measures[meas_id][2]
        measure_str = "(same)" if measure_title == outcome_title else measure_title
        if cat_ids is None:
            data_row = ct.get_outcome_measure_data(nct_dict, outcome_id, meas_id, None, ['O1', 'O2'])
            click.echo("- ({}) {} > {} ({}): {}".format(outcome_type, outcome_title, measure_str, measure_units, data_row))
        else:
            for cat_id in cat_ids:
                data_row = ct.get_outcome_measure_data(nct_dict, outcome_id, meas_id, cat_id, ['O1', 'O2'])
                click.echo("- ({}) {} > {} > {}: {}".format(outcome_type, outcome_title, measure_str, cat_ids, data_row))

    # * AEs
    click.echo("\n* Adverse Events:")
    ae_inputs = [(True, 1, 0),
                 (True, 2, 13),
                 (False, 2, 0)]
    ae_group_ids = ['E1', 'E2']
    for is_serious, cat_title, event_subtitle in ae_inputs:
        ae_data = ct.get_adverse_event_data(nct_dict, is_serious, cat_title, event_subtitle, ae_group_ids)
        is_serious_str = "Serious" if is_serious else "Other"
        click.echo("- ({}) {} > {}: {}".format(is_serious_str, cat_title, event_subtitle, ae_data))


if __name__ == '__main__':
    print_xml_data()
