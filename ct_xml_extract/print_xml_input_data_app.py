import click

from ct_xml_extract import ct_xml_utils as ct


@click.command()
@click.argument('nct_repo_dir')
@click.argument('nct_id')
def print_xml_data(nct_repo_dir, nct_id):
    """
    Prints most information from the passed nct_id xml file as made available by ct.py . A precursor to
    a web app that steps users through each section, collecting which information she wants to extract to a csv file.
    """
    click.echo("==== {} ====".format(nct_id))

    nct_dict = ct.dict_for_nct_id(nct_repo_dir, nct_id)

    # 2) Participant Flow > Reporting Groups:
    pf_groups = ct.get_participant_flow_groups(nct_dict)
    click.echo("\n* Reporting Groups:")
    for group_id, group_title, group_description in pf_groups:
        click.echo("- ({}) {}".format(group_id, group_title))

    # 3) Participant Flow > Periods:
    periods = ct.get_periods(nct_dict)
    click.echo("\n* Periods: {}".format(periods))

    # 4a) Participant Flow > Period 1: Treatment Period > Started/Completed/Not Completed:
    # 4b) Participant Flow > Period 2: Observational Period > Started/Completed/Not Completed:
    click.echo("\n* Period details:")
    for period in periods:
        click.echo("- {}:".format(period))
        milestones = ct.get_milestones(nct_dict, period[0])
        drop_withdraw_reasons = ct.get_drop_withdraw_reasons(nct_dict, period[0])
        click.echo("  = milestones={}".format(milestones))
        click.echo("  = drop_withdraw_reasons={}".format(drop_withdraw_reasons))

    # 5) Outcome Measures:
    outcomes = ct.get_outcomes(nct_dict)
    click.echo("\n* Outcome Measures ({}):".format(len(outcomes)))
    for oc_id, oc_title, oc_type in outcomes:
        click.echo("- {}: {} ({})".format(oc_id, oc_title, oc_type))

    # 6a) Outcome Measures > Other Outcome Measures > 1. Primary: Percentage of Participants With Symptomatic ...
    # 6b) Outcome Measures > Other Outcome Measures > 8. Other Pre-specified: Percentage of Participants With the ...
    click.echo("\n* Outcome Measure Details:")
    for oc_id, oc_title, oc_type in outcomes:
        click.echo("- {}: {} ({})".format(oc_id, oc_title, oc_type))
        for meas_id, meas_title, meas_units in ct.get_outcome_measures(nct_dict, oc_id):
            click.echo("  = {}: {} ({})".format(
                meas_id, meas_title if meas_title != oc_title else '<same>', meas_units))
            cats = ct.get_outcome_measure_cats(nct_dict, oc_id, meas_id)
            for category in cats:
                click.echo("    + {}".format(category))

    # 7) Serious Adverse Events:
    # 8) Other Adverse Events:
    adv_event_type_to_cats = ct.get_adverse_event_type_to_cats(nct_dict)
    for ae_type, adverse_events in adv_event_type_to_cats.items():
        click.echo("\n* {} ({} categories):".format(ae_type, len(adverse_events)))
        for ae_cat_id, ae_title in adverse_events:
            events = ct.get_adverse_event_cat_to_events(nct_dict, ae_type == 'serious_events', ae_cat_id)
            click.echo("- {}: {} ({})".format(ae_cat_id, ae_title, len(events)))
            for ae_cat in events:
                click.echo("  = {}".format(ae_cat))

    # 9) Done: [Submit button]


if __name__ == '__main__':
    print_xml_data()
