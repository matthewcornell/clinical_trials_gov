import itertools

from ct_xml_extract import ct_xml_utils as ct


def get_overall_study_rows(nct_id, nct_dict, pf_group_ids):
    """
    :param pf_group_ids: as returned by get_participant_flow_groups()
    :return the (fixed) four header rows for the 'Overall Study' section. this section has a fixed number of
        columns (9). format: a RxC list of lists where the outer list is the rows and the inner is each row's columns.
        only handles two pf_group_ids.
    """
    # check # groups
    if len(pf_group_ids) != 2:
        raise Exception("number of PF groups != 2: {}".format(pf_group_ids))

    # see if the design has the right information
    design_dict = ct.get_study_design(nct_dict)
    try:
        design_list = [design_dict['Allocation'], design_dict['Endpoint Classification'],
                       design_dict['Intervention Model'], design_dict['Primary Purpose']]
    except KeyError as ke:
        raise Exception("design_dict did not have a required field: {}".format(ke))

    # set header rows
    header_rows = [['Overall Study', '', '', '', '', '', '', '', ''],
                   ['', '', 'Design', '', '', '', '', 'Interventions', ''],
                   ['NCT ID', 'Study Type', 'Allocation', 'Endpoint Classification', 'Intervention Model',
                    'Primary Purpose', 'Condition', 'Group 1', 'Group 2']]

    # get the data
    pf_groups = ct.get_participant_flow_groups(nct_dict)
    pf_group_0 = [pf_group_id_title_descr for pf_group_id_title_descr in pf_groups
                  if pf_group_id_title_descr[0] == pf_group_ids[0]][0]
    pf_group_1 = [pf_group_id_title_descr for pf_group_id_title_descr in pf_groups
                  if pf_group_id_title_descr[0] == pf_group_ids[1]][0]
    data_row = [nct_id, ct.get_study_type(nct_dict), *design_list, ct.get_study_condition(nct_dict),
                pf_group_0[1], pf_group_1[1]]

    # done
    return header_rows + [data_row]


def get_participant_flow_rows(nct_dict, period_id, milestone_ids, dw_reason_ids, pf_group_ids):
    """
    :param milestone_ids: milestones to retrieve
    :param dw_reason_ids: drop_withdraw_reason ids
    :param pf_group_ids: as returned by get_participant_flow_groups(). Must be exactly two of them.
    :return list similar to get_overall_study_rows() but for the Participant Flow section. This section has a variable
    number of columns, depending on the number of requested milestone_ids and dw_reason_ids. Only handles two
    pf_group_ids.
    """
    # check # groups
    if len(pf_group_ids) != 2:
        raise Exception("number of PF groups != 2: {}".format(pf_group_ids))

    header_row_0 = []
    header_row_1 = []
    header_row_2 = []
    data_row = []

    # add dosage regimen
    header_row_0.extend(['Participant Flow', ''])
    header_row_1.extend(['Dosage Regimen', ''])
    header_row_2.extend(['Group 1', 'Group 2'])
    pf_groups = ct.get_participant_flow_groups(nct_dict)
    pf_group_0 = [pf_group_id_title_descr for pf_group_id_title_descr in pf_groups
                  if pf_group_id_title_descr[0] == pf_group_ids[0]][0]
    pf_group_1 = [pf_group_id_title_descr for pf_group_id_title_descr in pf_groups
                  if pf_group_id_title_descr[0] == pf_group_ids[1]][0]
    data_row.extend([pf_groups[0][2], pf_groups[1][2]])

    # add milestones. note that we preserve the order of milestones, which is always (?) 'STARTED', ..., 'COMPLETED',
    # 'NOT COMPLETED'
    pf_group_ids = [pf_group_0[0], pf_group_1[0]]
    milestones = ct.get_milestones(nct_dict, period_id)
    milestone_data = ct.get_milestone_data(nct_dict, period_id, milestone_ids, pf_group_ids)
    for idx, milestone_id in enumerate(milestone_ids):
        ms_data_row = milestone_data[idx]
        milestone_title = milestones[milestone_id][1]
        header_row_0.extend(['', ''])
        header_row_1.extend([milestone_title, ''])
        header_row_2.extend(['Group 1', 'Group 2'])
        data_row.extend(ms_data_row)

    # add drop/withdraw reasons
    dw_reasons = ct.get_drop_withdraw_reasons(nct_dict, period_id)
    drop_withdraw_data = ct.get_drop_withdraw_data(nct_dict, period_id, dw_reason_ids, pf_group_ids)
    for idx, dw_reason_id in enumerate(dw_reason_ids):
        dw_data_row = drop_withdraw_data[idx]
        dw_reason_title = dw_reasons[dw_reason_id][1]
        header_row_0.extend(['', ''])
        header_row_1.extend([dw_reason_title, ''])
        header_row_2.extend(['Group 1', 'Group 2'])
        data_row.extend(dw_data_row)

    # done
    return [header_row_0, header_row_1, header_row_2, data_row]


def get_baseline_measures_rows(nct_dict, bl_group_ids):
    """
    :param bl_group_ids: List of baseline group ids. Must be exactly two of them.
    :return list similar to get_overall_study_rows() but for the Baseline Measures section. this section has a
        fixed number of columns (8). Only handles two bl_group_ids.

    """
    # check # groups
    if len(bl_group_ids) != 2:
        raise Exception("number of BL groups != 2: {}".format(bl_group_ids))

    # set header rows
    bl_measure_titles = ['Number of Participants', 'Age', 'Gender']  # fixed choices
    header_rows = [['Baseline Measures', '', '', '', '', '', '', ''],
                   ['Number of Participants', '', 'Age', '', 'Female', '', 'Male', ''],
                   ['Group 1', 'Group 2', 'Group 1', 'Group 2', 'Group 1', 'Group 2', 'Group 1', 'Group 2']]

    # get the data
    baseline_data = ct.get_baseline_data(nct_dict, bl_measure_titles, bl_group_ids)
    data_row = baseline_data['Number of Participants'] + baseline_data['Age'] + baseline_data['Female'] + \
               baseline_data['Male']

    # done
    return header_rows + [data_row]


def get_outcome_measure_rows(nct_dict, oc_id_cat_ids, oc_group_ids):
    """
    Returns a list of datasimilar to get_overall_study_rows() but for the Outcome Measures section. This section has a
    variable number of columns, depending on the number of requested title_measure_subtitle_pairs. The measure_id is
    *not* passed in oc_id_cat_ids b/c all outcomes have two measures, where the first is always
    'Number of Participants' and the second is the 'actual' measure. This first outcome is the one we've decided to use
    to get the denominators for, and the second for the numerators.

    :param oc_id_cat_ids: a list of 2-tuples of the form: (outcome_id, [cat_subtitle_id_0, ...]).
        [cat_subtitle_id_0, ...] list is [] if there are no subtitles in the category.
    :param oc_group_ids: as returned by get_outcome_groups(). Must be exactly two of them.
    """
    # check # groups
    if len(oc_group_ids) != 2:
        raise Exception("number of OC groups != 2: {}".format(oc_group_ids))

    header_row_0 = []
    header_row_1 = []
    header_row_2 = []
    data_row = []

    # add outcome measures
    for idx, (outcome_id, cat_subtitle_ids) in enumerate(oc_id_cat_ids):
        # get outcome_type
        try:
            outcome_type = ct.get_outcomes(nct_dict)[outcome_id][2]
        except IndexError:
            raise Exception("no outcome found for outcome_id={}".format(outcome_id))

        # get units. we do not expect IndexErrors b/c there are always two measures
        meas_units_1 = ct.get_outcome_measures(nct_dict, outcome_id)[1][2]
        if meas_units_1 == 'Percentage of participants':
            meas_units_1 = '%'

        # get rows for both measures
        oc_data_row_meas_0 = ct.get_outcome_measure_data(nct_dict, outcome_id, 0, None, oc_group_ids)
        if not cat_subtitle_ids:
            oc_data_row_meas_1 = ct.get_outcome_measure_data(nct_dict, outcome_id, 1, None, oc_group_ids)
            header_row_0.extend(['Outcome {} ({})'.format(idx + 1, outcome_type), '', '', ''])
            header_row_1.extend(['Category {} ({})'.format(idx + 1, meas_units_1), '', '', ''])
            header_row_2.extend(['Group 1: Num', 'Group 1: Denom', 'Group 2: Num', 'Group 2: Denom'])
            data_row.extend(itertools.chain.from_iterable(zip(oc_data_row_meas_1, oc_data_row_meas_0)))  # flatten
        else:
            for cat_idx, cat_subtitle_id in enumerate(cat_subtitle_ids):
                oc_data_row_cat = ct.get_outcome_measure_data(nct_dict, outcome_id, 1, cat_subtitle_id, oc_group_ids)
                header_row_0.extend(['Outcome {} ({})'.format(idx + 1, outcome_type), '', '', ''])
                header_row_1.extend(['Category {} ({})'.format(cat_idx + 1, meas_units_1), '', '', ''])
                header_row_2.extend(['Group 1: Num', 'Group 1: Denom', 'Group 2: Num', 'Group 2: Denom'])
                data_row.extend(itertools.chain.from_iterable(zip(oc_data_row_cat, oc_data_row_meas_0)))  # flatten

    # done
    return [header_row_0, header_row_1, header_row_2, data_row]


def get_adverse_event_rows(nct_dict, serious_cat_event_ids, ae_group_ids):
    """
    :param serious_cat_event_ids: a list of 3-tuples of the form: (is_serious, cat_id, event_id).
        is_serious: boolean for which AE type (serious or other), cat_id ids the AE category, and event_id ids the AE
        subtitle.
    :param ae_group_ids: List of outcome group ids. Must be exactly two of them.
    :return list similar to get_overall_study_rows() but for the Adverse Events section. this section has a variable
        number of columns, depending on the number of requested serious_cat_event_ids.
    """
    # check # groups
    if len(ae_group_ids) != 2:
        raise Exception("number of AE groups != 2: {}".format(ae_group_ids))

    header_row_0 = []
    header_row_1 = []
    header_row_2 = []
    data_row = []

    # add adverse events
    for idx, (is_serious, cat_id, event_id) in enumerate(serious_cat_event_ids):
        ae_data = ct.get_adverse_event_data(nct_dict, is_serious, cat_id, event_id, ae_group_ids)
        header_row_0.extend(['Adverse Events', '', '', ''])
        header_row_1.extend(['Outcome Measure {}'.format(idx + 1), '', '', ''])
        header_row_2.extend(['Group 1: Affected', 'Group 1: At Risk', 'Group 2: Affected', 'Group 2: At Risk'])
        data_row.extend(itertools.chain.from_iterable(ae_data))  # flatten

    # done
    return [header_row_0, header_row_1, header_row_2, data_row]
