import os
from ct_xml_extract import ct_xml_utils as ct

import click


@click.command()
@click.argument('nct_repo_dir')
def extract_descriptions(nct_repo_dir):
    """
    Extracts description fields from all *.xml files in nct_repo_dir using ct.get_participant_flow_groups(), saving each
    group_id-description pair in a separate file in the same directory with the naming scheme:
    '<nct_id>-<group_id>.txt' .
    
    :param nct_repo_dir: 
    """
    click.echo("Starting exporting files in '{}'".format(nct_repo_dir))
    for filename in os.listdir(nct_repo_dir):
        filename_split = filename.split('.')
        if os.path.isdir(filename) or (filename_split[-1] != 'xml') or (len(filename_split) != 2):
            continue

        click.echo("- processing '{}'".format(filename))
        try:
            nct_id = filename_split[0]
            nct_dict = ct.dict_for_nct_id(nct_repo_dir, nct_id)
            re_groups = ct.get_participant_flow_groups(nct_dict)
            for group_id, _, group_description in re_groups.items():
                txt_filename = "{}-{}.txt".format(nct_id, group_id)
                with open(os.path.join(nct_repo_dir, txt_filename), 'w') as txt_file:
                    txt_file.write(group_description)
        except Exception as exc:
            click.echo("  = skipping: error: '{}'".format(exc))


if __name__ == '__main__':
    extract_descriptions()
