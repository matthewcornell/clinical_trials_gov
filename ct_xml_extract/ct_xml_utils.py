import ast
import os

import xmltodict


#
# dict_for_nct_id
#

def dict_for_nct_id(nct_repo_dir, nct_id):
    """
    :param nct_id: trial id string, e.g., 'NCT00440193'. must match the corresponding xml file, i.e.,
        'NCT00440193.xml' in this case.
    :return: a dict for nct_id's xml file
    """
    xml_file_name = nct_id + '.xml'
    xml_file_abs_path = os.path.join(nct_repo_dir, xml_file_name)
    with open(xml_file_abs_path) as xml_file:
        nct_dict = xmltodict.parse(xml_file.read())
        # with open('/tmp/' + nct_id + '.json', 'w') as file:
        #     json.dump(nct_dict, file, indent=4)
        return nct_dict


#
# study type, condition, design
#

def get_study_type(nct_dict):
    return nct_dict['clinical_study']['study_type']


def get_study_condition(nct_dict):
    return nct_dict['clinical_study']['condition']


def get_study_design(nct_dict):
    """
    Parses the raw study_design text to extract and return a dict with the corresponding key/value fields. The key
    is lower-cased. Note that studies can have different fields, and therefore different design dicts. That said,
    some common ones are: 'Allocation', Endpoint Classification', 'Intervention Model', and 'Primary Purpose' (but
    sometimes not, for that one).

    """
    return split_study_design_str(nct_dict['clinical_study']['study_design'])


def split_study_design_str(study_design_str):
    key_val_pairs = []
    for colon_split in study_design_str.split(':'):
        last_comma_idx = colon_split.rfind(',')
        if last_comma_idx == -1:
            key_val_pairs.append(colon_split.strip())
        else:
            key_val_pairs.append(colon_split[:last_comma_idx].strip())
            key_val_pairs.append(colon_split[last_comma_idx + 1:].strip())
    even_odd_zip = zip([key for idx, key in enumerate(key_val_pairs) if idx % 2 == 0],
                       [val for idx, val in enumerate(key_val_pairs) if idx % 2 != 0])
    return {x: y for x, y in even_odd_zip}


#
# reporting groups
#
# NB: For reference, there are often multiple 'groups' within a file (e.g., "P1" vs "B1"), which was confusing to me.
# Most types of data have an explicit 'group_list' entry, say for NCT00643201:
#   /clinical_study/clinical_results/baseline/group_list          -> "B1", "B2" (titles: "")
#   /clinical_study/clinical_results/participant_flow/group_list  -> "P1", "P2" (titles: 'Apixaban', 'Enoxaparin + Warfarin')
#   /clinical_study/clinical_results/reported_events/group_list   -> "E1", "E2" (titles: "")
#
# Outcomes, however, unfortunately have no such central list. Instead, group_list is repeated for /every outcome/ :-/
# Again, for NCT00643201:
#   /clinical_study/clinical_results/outcome_list/outcome[1]/group_list  -> "O1", "O2" (titles: "")
#   /clinical_study/clinical_results/outcome_list/outcome[2]/group_list  -> ""
#   ...
#   /clinical_study/clinical_results/outcome_list/outcome[21]/group_list  -> ""
#
# So, get_baseline_groups() arbitrarily returns the first as the canonical one.
#

def get_baseline_groups(nct_dict):
    """
    :param nct_dict:
    :return: list of 2-tuples mapping group_id -> group_title for baseline groups. we use a list instead of a dict b/c
        preserving order is crucial
    """
    groups = nct_dict['clinical_study']['clinical_results']['baseline']['group_list']['group']
    return [(group['@group_id'], group['title']) for group in groups]


def get_outcome_groups(nct_dict):
    """
    Similar to get_baseline_groups() but for outcome groups, However, arbitrarily returns the first as the canonical
    one (see note above).
    """
    # /clinical_study/clinical_results/outcome_list/outcome[1]/group_list
    outcomes = nct_dict['clinical_study']['clinical_results']['outcome_list']['outcome']
    if not outcomes:
        raise RuntimeError("no outcomes found for nct_dict")

    groups = outcomes[0]['group_list']['group']
    return [(group['@group_id'], group['title']) for group in groups]


def get_participant_flow_groups(nct_dict):
    """
    Similar to get_baseline_groups() but for participant_flow groups, EXCEPT that it returns a 3-tuple that also
    includes the description.
    """
    groups = nct_dict['clinical_study']['clinical_results']['participant_flow']['group_list']['group']
    return [(group['@group_id'], group['title'], group['description']) for group in groups]


def get_reported_event_groups(nct_dict):
    """
    Similar to get_baseline_groups() but for reported_events groups.
    """
    groups = nct_dict['clinical_study']['clinical_results']['reported_events']['group_list']['group']
    return [(group['@group_id'], group['title']) for group in groups]


#
# periods
#

def get_periods(nct_dict):
    """
    :return period information for nct_id as a list of 2-tuples of the form: (index, title)
    """
    return list(enumerate([period['title'] for period in get_period_list(nct_dict)]))


def get_period_list(nct_dict):
    periods = nct_dict['clinical_study']['clinical_results']['participant_flow']['period_list']['period']
    # two cases: periods is a dict or a list of dicts
    if not isinstance(periods, list):
        periods = [periods]
    return periods


def get_milestones(nct_dict, period_id):
    """
    :return milestone information for period_id as a list of 2-tuples of the form: (index, title)
    """
    period = get_period_list(nct_dict)[period_id]
    milestones = period['milestone_list']['milestone']
    return list(enumerate([milestone['title'] for milestone in milestones]))


def get_drop_withdraw_reasons(nct_dict, period_id):
    """
    :return drop-withdraw information for period_id as a list of 2-tuples of the form: (index, title)
    """
    period = get_period_list(nct_dict)[period_id]
    drop_withdraw_reasons = period['drop_withdraw_reason_list']['drop_withdraw_reason']
    return list(enumerate([milestone['title'] for milestone in drop_withdraw_reasons]))


#
# milestone & drop_withdraw data
#

def get_milestone_data(nct_dict, period_id, milestone_ids, pf_group_ids):
    """
    Returns a table of milestone data corresponding to the inputs, essentially the Participant Flow table in
    nct_dict's web page section for the passed period. The table is a list of rows, one row per milestone_ids, and
    each row is a list of numbers, one column for each pf_group_ids.

    :param period_id: as returned by get_periods()
    :param milestone_ids: as returned by get_milestones()
    :param pf_group_ids: list of participant flow group ids as returned by get_participant_flow_groups()
    :return: data table
    """
    data = []
    period = get_period_list(nct_dict)[period_id]
    milestones = period['milestone_list']['milestone']
    for idx, milestone in enumerate(milestones):
        if idx in milestone_ids:
            data.append(data_row_for_period_list_item(milestone, pf_group_ids))
    return data


def get_drop_withdraw_data(nct_dict, period_id, dw_reason_ids, pf_group_ids):
    """
    Similar to get_milestone_data().

    :param dw_reason_ids: as returned by get_drop_withdraw_reasons()
    """
    data = []
    period = get_period_list(nct_dict)[period_id]
    dw_reasons = period['drop_withdraw_reason_list']['drop_withdraw_reason']
    for idx, dw_reason in enumerate(dw_reasons):
        if idx in dw_reason_ids:
            data.append(data_row_for_period_list_item(dw_reason, pf_group_ids))
    return data


def data_row_for_period_list_item(period_list_item, pf_group_ids):
    """
    :param period_list_item: a dict from either of these types:
        - clinical_study.clinical_results.participant_flow.period_list.period.milestone_list.milestone
        - clinical_study.clinical_results.participant_flow.period_list.period.drop_withdraw_reason_list.drop_withdraw_reason
    :param pf_group_ids: list of arm_group_labels, i.e., the values of the get_arm_groups()
    :return: a row of @count data for period_list_item, with len(pf_group_ids) columns, one for each group
    """
    row = []
    for participant in period_list_item['participants_list']['participants']:
        group_id, count = participant['@group_id'], participant['@count']
        if group_id in pf_group_ids:
            row.append(ast.literal_eval(count))
    return row


def get_baseline_data(nct_dict, measure_titles, bl_group_ids):
    """
    Similar to above *_data() methods.

    :param measure_titles:
    :param bl_group_ids: as returned by get_baseline_groups()
    :return: a data dict similar to get_milestone_data(), but for baseline data. NB: for those measures that have
        sub_titles, we use each sub_title instead, which means the passed measurement_name will *not* appear as
        a key in the data dict - see the tests
    """
    data = {}
    measures = nct_dict['clinical_study']['clinical_results']['baseline']['measure_list']['measure']
    for measure in measures:
        if measure['title'] in measure_titles:
            categories = measure['category_list']['category']
            # two cases: categories is a dict or a list of dicts
            if isinstance(categories, list):  # case 1 -> we have sub_titles for each measurement_list
                for category_item in categories:
                    measurements = category_item['measurement_list']['measurement']
                    data[category_item['sub_title']] = data_row_for_measurements(measurements, bl_group_ids)
            else:  # case 2 -> a single measurement_list with no sub_titles, so use the measure's title
                category_item = categories
                measurements = category_item['measurement_list']['measurement']
                data[measure['title']] = data_row_for_measurements(measurements, bl_group_ids)

    return data


def data_row_for_measurements(measurements, bl_group_ids):
    """
    :return: a list of len(bl_group_ids) @value data, one for each measurement item in measurements
    """
    row = []
    for measurement in measurements:
        group_id, value = measurement['@group_id'], measurement['@value']
        if group_id in bl_group_ids:
            row.append(ast.literal_eval(value))
    return row


#
# outcomes
#

def get_outcomes(nct_dict):
    """
    :return: outcome information as list of 3-tuples of the form: (index, title, type), where type is one of: 'Primary',
        'Secondary', 'Other Pre-specified', 'Post-Hoc'.
    """
    outcomes = nct_dict['clinical_study']['clinical_results']['outcome_list']['outcome']
    title_types = [(outcome['title'], outcome['type']) for outcome in outcomes]
    return enumerate_flattened_pairs(title_types)


def get_outcome_measures(nct_dict, outcome_id):
    """
    :return outcome measure information for outcome_id as a list of 3-tuples of the form: (index, title, units)
    """
    outcome = nct_dict['clinical_study']['clinical_results']['outcome_list']['outcome'][outcome_id]
    if 'measure_list' in outcome:
        measures = outcome['measure_list']['measure']
        title_units = [(outcome['title'], outcome['units']) for outcome in measures]
        return enumerate_flattened_pairs(title_units)
    else:
        return []


def get_outcome_measure_cats(nct_dict, outcome_id, measure_id):
    """
    :return measure category information for outcome_id and measure_id as a list of 2-tuples of the form:
        (index, sub_title)
    """
    outcome = nct_dict['clinical_study']['clinical_results']['outcome_list']['outcome'][outcome_id]
    measure = outcome['measure_list']['measure'][measure_id]
    categories = measure['category_list']['category']
    # two cases: 1) category list a list of dicts with 'sub_title' keys, or 2) it is a single dict with a
    # 'measurement_list' key. #2 is typical for 'Number of Participants' measures. for case 1 we use a list of
    # the 'sub_title' values. for case 2 we use []
    if isinstance(categories, list):  # case 1
        cat_list = [cat['sub_title'] for cat in categories]
    else:
        cat_list = []
    return list(enumerate(cat_list))


def enumerate_flattened_pairs(two_tuples):
    return [(idx_tuples[0], idx_tuples[1][0], idx_tuples[1][1]) for idx_tuples in (enumerate(two_tuples))]


def get_outcome_measure_data(nct_dict, outcome_id, measure_id, category_id, oc_group_ids):
    """
    Returns a single row of baseline data corresponding to the args, one number per oc_group_ids.
    Note that this method's output differs from other *_data() methods in that it only returns one row rather than a
    dict. This is motivated by the complexity of the xml schema's handling of subcategories - some measures have them
    but some do not.

    :param measure_id:
    :param category_id: None if measure_id is not broken down into subcategories. o/w the subcategory id
    :param oc_group_ids: as returned by get_outcome_groups()
    :return: data row
    """
    outcome = nct_dict['clinical_study']['clinical_results']['outcome_list']['outcome'][outcome_id]
    measure = outcome['measure_list']['measure'][measure_id]
    categories = measure['category_list']['category']
    # two cases: 1) categories is a list of dicts with 'sub_title' keys, or 2) it is a single dict with a 'sub_title' key
    if isinstance(categories, list):  # case 1 -> we have sub_titles for each measurement_list. use category_id
        if category_id is None:
            raise Exception("found a list of categories, but was not passed a category_id")

        measurements = categories[category_id]['measurement_list']['measurement']
        return data_row_for_measurements(measurements, oc_group_ids)
    else:  # case 2 -> a single measurement_list with no sub_titles, so use the outcome's title. ignore category_id
        measurements = categories['measurement_list']['measurement']
        return data_row_for_measurements(measurements, oc_group_ids)


#
# adverse events
#

def get_adverse_event_type_to_cats(nct_dict):
    """
    :return AE category (i.e., top-level) information as a dict mapping adverse event type (either 'serious_events' or
         'other_events') to a list of 2-tuples of the form: (index, title)
    """
    reported_events = nct_dict['clinical_study']['clinical_results']['reported_events']
    serious_event_cats = reported_events['serious_events']['category_list']['category'] \
        if 'serious_events' in reported_events \
        else []
    other_event_cats = reported_events['other_events']['category_list']['category'] \
        if 'other_events' in reported_events \
        else []
    return {
        'serious_events': list(enumerate([cat['title'] for cat in serious_event_cats])),
        'other_events': list(enumerate([cat['title'] for cat in other_event_cats])),
    }


def get_adverse_event_cat_to_events(nct_dict, is_serious, category_id):
    """
    :param is_serious: True if category_id is in 'serious_events', False if 'other_events'
    :param category_id: category to get the event listing of
    :return AE category event information as a list of 2-tuples of the form: (index, sub_title)
    """
    reported_events = nct_dict['clinical_study']['clinical_results']['reported_events']
    categories = reported_events['serious_events' if is_serious else 'other_events']['category_list']['category']

    # two cases: events is a dict or a list of dicts
    events = categories[category_id]['event_list']['event']
    if not isinstance(events, list):
        events = [events]
    return list(enumerate([event['sub_title'] for event in events]))


#
# adverse events data
#

def get_adverse_event_data(nct_dict, is_serious, category_id, event_id, ae_group_ids):
    """
    :param is_serious: True for serious_events, false for other_events
    :param ae_group_ids: as returned by get_reported_event_groups()
    :return: a list of len(ae_group_ids) count data for counts, each being a 2-tuple of the form:
        (@subjects_affected, @subjects_at_risk).
    """
    reported_events = nct_dict['clinical_study']['clinical_results']['reported_events']
    results_type = 'serious_events' if is_serious else 'other_events'
    category = reported_events[results_type]['category_list']['category'][category_id]
    events = category['event_list']['event']
    # two cases: events is a dict or a list of dicts
    if not isinstance(events, list):
        events = [events]
    return data_row_for_event_counts(events[event_id]['counts'], ae_group_ids)


def data_row_for_event_counts(counts, ae_group_ids):
    """
    :return: as specified in get_adverse_event_data()
    """
    row = []
    for count in counts:
        group_id, affected, at_risk = count['@group_id'], count['@subjects_affected'], count['@subjects_at_risk']
        if group_id in ae_group_ids:
            row.append([ast.literal_eval(affected), ast.literal_eval(at_risk)])
    return row
