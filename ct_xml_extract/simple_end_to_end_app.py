import csv

import click

from ct_xml_extract import ct_xml_csv_output as ct_csv
from ct_xml_extract import ct_xml_utils as ct


def print_data():
    # user input as a dict
    query = {
        'nct_id': 'NCT00440193',
        'pf_group_ids': ['P1', 'P2'],
        'bl_group_ids': ['B1', 'B2'],
        'oc_group_ids': ['O1', 'O2'],
        'ae_group_ids': ['E1', 'E2'],
        'period_id': 0,
        'milestone_ids': [0, 2, 3],
        'dw_reason_ids': [0, 1],
        'oc_id_cat_ids': [(0, []), (7, [0, 1])],
        'serious_cat_event_ids': [(True, 1, 0), (True, 2, 13), (False, 2, 0)],
    }

    # get the data
    nct_dict = ct.dict_for_nct_id('test', query['nct_id'])
    overall_study_rows = ct_csv.get_overall_study_rows(query['nct_id'], nct_dict, query['pf_group_ids'])
    pf_rows = ct_csv.get_participant_flow_rows(nct_dict, query['period_id'], query['milestone_ids'],
                                               query['dw_reason_ids'], query['pf_group_ids'])
    bl_rows = ct_csv.get_baseline_measures_rows(nct_dict, query['bl_group_ids'])
    oc_rows = ct_csv.get_outcome_measure_rows(nct_dict, query['oc_id_cat_ids'], query['oc_group_ids'])
    ae_rows = ct_csv.get_adverse_event_rows(nct_dict, query['serious_cat_event_ids'], query['ae_group_ids'])
    act_final_rows = [overall_study_rows[0] + pf_rows[0] + bl_rows[0] + oc_rows[0] + ae_rows[0],
                      overall_study_rows[1] + pf_rows[1] + bl_rows[1] + oc_rows[1] + ae_rows[1],
                      overall_study_rows[2] + pf_rows[2] + bl_rows[2] + oc_rows[2] + ae_rows[2],
                      overall_study_rows[3] + pf_rows[3] + bl_rows[3] + oc_rows[3] + ae_rows[3]]

    # done!
    tsv_file_name = '/tmp/out.tsv'
    click.echo("Writing rows to file: {}".format(tsv_file_name))
    with open(tsv_file_name, 'w', newline='') as tsv_file:
        writer = csv.writer(tsv_file, dialect=csv.excel_tab)
        for row in act_final_rows:
            click.echo("writing row: {}".format(row))
            writer.writerow(row)
    click.echo("Done!")


if __name__ == '__main__':
    print_data()
