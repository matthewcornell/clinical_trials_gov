import csv
import itertools
import os

import click
import mysql.connector


@click.command()
@click.argument('output_dir')
@click.argument('nct_id')
def extract_adverse_events(output_dir, nct_id):
    """
    Extracts all adverse event data for nct_id, using the MySQL database 'clinical_trials_gov' as described in
    readme.md . Output format is a TSV file named <output_dir>/<nct_id>-adverse-events.tsv in the format demonstrated
    in extract-adverse-events-example-translation.xlsx.

    Note that there are four lines of headers: 1) general trial information, 2) blank line, 3) indented per
    arm_group_labels, and 4) subjs_affect & subjs_risk pairs, one per arm_group_label. for example ('-' and '|' added
    here for visual clarity):

    --------------------------------------------------------------------------------------------------------------------------------
    <trial info>  |                  |                  |          |                     |          |                    |
                  |                  |                  |          |                     |          |                    |
                  |                  |Imatinib 400 mg QD|          |Nilotinib 300 mg BID |          |Nilotinib 400 mg BID|
    category_title|category_sub_title|subjs_affect      |subjs_risk|subjs_affect         |subjs_risk|subjs_affect        |subjs_risk
    --------------------------------------------------------------------------------------------------------------------------------

    :param: output_dir: TSV file to save to
    :param: nct_id: clinical_study nct_id to print info for

    todo percentages
    todo: where is 'Discontinue due to AE'?
    
    """
    conn = mysql.connector.connect(user='root', password='myegan', host='127.0.0.1', database='clinical_trials_gov')
    click.echo("Extracting data for trial nct_id='{}'".format(nct_id))
    if not is_nct_id_exists(conn, nct_id):
        click.echo("No trial found for nct_id='{}'".format(nct_id), err=True)
        return

    arm_group_labels = get_reported_event_arm_group_labels(conn, nct_id)
    click.echo("Trial has {} arm group labels: |{}|".format(
            len(arm_group_labels), '|'.join(arm_group_labels)))

    adverse_events = get_adverse_events(conn, nct_id)
    output_file = os.path.join(output_dir, nct_id + '-adverse-events.tsv')
    click.echo("Found {} raw adverse event rows".format(len(adverse_events)))
    click.echo("Writing adverse event data to {}".format(output_file))

    with open(output_file, 'w', newline='') as csvfile:
        writer = csv.writer(csvfile, dialect=csv.excel_tab)

        # write the header rows
        header = [get_general_trial_info_str(conn, nct_id)]
        writer.writerow(header)

        header = [None] * (3 + len(arm_group_labels))
        writer.writerow(header)

        header = [None, None]
        for arm_group_label in arm_group_labels:
            header.extend([arm_group_label, None])
        writer.writerow(header)

        header = ['category_title', 'category_sub_title']
        for __ in range(len(arm_group_labels)):
            header.extend(['subjects_affected', 'subjects_at_risk'])
        writer.writerow(header)

        # write the data
        grouped_adverse_events = group_adverse_events(adverse_events)
        for row in grouped_adverse_events:
            writer.writerow(row)

    # done
    click.echo("Done: wrote {} final rows".format(len(grouped_adverse_events)))


def is_nct_id_exists(conn, nct_id):
    """
    :return: 1 if nct_id is found in the database, 0 o/w
    """
    query = """
        SELECT count(*)
        FROM clinical_study
        WHERE NCT_ID = %s;
    """
    curs = conn.cursor()
    curs.execute(query, (nct_id,))
    rows = curs.fetchone()
    curs.close()
    return int(rows[0])


def get_reported_event_arm_group_labels(conn, nct_id):
    """
    :return: list of 'Reported Event' ARM_GROUP_LABELs for nct_id
    """
    query = """
        SELECT ag.ARM_GROUP_LABEL
        FROM arm_groups ag
        WHERE ag.nct_id = %s
              AND ag.ARM_GROUP_TYPE = 'Reported Event'
        ORDER BY ag.arm_group_id;
    """
    curs = conn.cursor()
    curs.execute(query, (nct_id,))
    rows = curs.fetchall()
    curs.close()
    return [row[0] for row in rows]


def get_adverse_events(conn, nct_id):
    """
    NB: We re.EVENT_TYPE, which means cat/subcat combinations are merged for 'Serious' and 'Other'

    :return: a 6-tuple of AE strings for nct_id of the following form, ordered by the first three:
        (CATEGORY_TITLE, CATEGORY_SUB_TITLE, ARM_GROUP_LABEL, SUBJECTS_AFFECTED, SUBJECTS_AT_RISK)
        The ordering is consistent with get_reported_event_arm_group_labels().
    """
    query = """
        SELECT
          rec.CATEGORY_TITLE,
          rec.CATEGORY_SUB_TITLE,
          ag.ARM_GROUP_LABEL,
          recg.SUBJECTS_AFFECTED,
          recg.SUBJECTS_AT_RISK
        FROM reported_events re
          JOIN reported_event_ctgy rec ON re.reported_event_id = rec.reported_event_id
          JOIN reported_event_ctgy_grp recg ON rec.reported_event_category_id = recg.reported_event_category_id
          JOIN arm_groups ag ON recg.arm_group_id = ag.arm_group_id
        WHERE re.nct_id = %s
              AND ag.ARM_GROUP_TYPE = 'Reported Event'
        ORDER BY re.REPORTED_EVENT_ID, rec.REPORTED_EVENT_CATEGORY_ID, ag.ARM_GROUP_ID;
    """
    curs = conn.cursor()
    curs.execute(query, (nct_id,))
    rows = curs.fetchall()
    curs.close()
    return rows


def get_general_trial_info_str(conn, nct_id):
    """
    :return: a string describing nct_id in general
    """
    query = """
        SELECT cs.DOWNLOAD_DATE, cs.BRIEF_TITLE, cs.ACRONYM, cs.SOURCE, cs.START_DATE, cs.COMPLETION_DATE,
          cs.COMPLETION_DATE_TYPE, cs.PHASE
        FROM clinical_study cs
        WHERE NCT_ID = %s;
    """
    curs = conn.cursor()
    curs.execute(query, (nct_id,))
    row = curs.fetchone()
    curs.close()
    download_date, brief_title, acronym, source, start_date, completion_date, completion_date_type, phase = row
    return "General study information for nct_id={}: download_date={}. brief_title={}. acronym={}. source={}. " \
           "start_date={}. completion_date={}. completion_date_type={}. phase={}".format(
            nct_id, download_date, brief_title, acronym, source, start_date, completion_date, completion_date_type, phase)


def group_adverse_events(adverse_events):
    """
    'Pivots' the input's categories and subcategories into a list of groups, one per line, as demonstrated in
    extract-adverse-events-example-translation.xlsx. Each returned row is a 4+-tuple of the form:

        (category, subcategory, num_subjects_affected_group_1, num_subjects_at_risk_group_1,
                                [num_subjects_affected_group_2, num_subjects_at_risk_group_2],
                                ...)

    The actual length of the tuple depends on the number of 'intervention arm groups' in the nct_id, all of which
    (presumably) have at least one of them.

    TODO: calculate percentage

    :param: adverse_events: as returned by get_adverse_events()
    :return: see description above
    """
    grouped_adverse_events = []
    for k, g in itertools.groupby(adverse_events, key=lambda ae_row: (ae_row[0], ae_row[1])):
        # start with the cat and subcat
        grouped_adverse_events_row = [k[0], k[1]]

        # add subjects_affected and subjects_at_risk for each intervention arm group
        g_rows = list(g)
        for g_row in g_rows:
            grouped_adverse_events_row.extend([int(g_row[3]), int(g_row[4])])
        grouped_adverse_events.append(grouped_adverse_events_row)
    return grouped_adverse_events


if __name__ == '__main__':
    extract_adverse_events()
