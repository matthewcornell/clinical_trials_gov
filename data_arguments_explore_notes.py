# NCT00643201

#
# participant_flow: milestones
# period_title, milestone_title, group_ids
#

('Randomized, Completed 6 Months of Study', 'STARTED', ['P1', 'P2'])
# [2691, 2704]
('Randomized, Completed 6 Months of Study', 'COMPLETED', ['P1', 'P2'])
# [2314, 2291]
('Randomized, Completed 6 Months of Study', 'NOT COMPLETED', ['P1', 'P2'])
# [377, 413]

# -> simplifying: period_title, milestone_titles, group_ids
('Randomized, Completed 6 Months of Study', ['STARTED', 'COMPLETED', 'NOT COMPLETED'], ['P1', 'P2'])
# {'STARTED': [2691, 2704],
#  'COMPLETED': [2314, 2291],
#  'NOT COMPLETED': [377, 413]}


#
# participant_flow: drop_withdraw_reasons
# period_title, drop_withdraw_reason_title, group_ids
#

('Randomized, Completed 6 Months of Study', 'Death', ['P1', 'P2'])
# [20, 26]
('Randomized, Completed 6 Months of Study', 'Adverse Event', ['P1', 'P2'])
# [150, 182]

# -> simplifying: period_title, drop_withdraw_reason_titles, group_ids
('Randomized, Completed 6 Months of Study', ['Death', 'Adverse Event'], ['P1', 'P2'])
# {'Death': [20, 26],
#  'Adverse Event': [150, 182]}


#
# baseline: measure_titles, group_ids
#

(['Number of Participants', 'Age', 'Gender'], ['B1', 'B2'])
# {"Number of Participants": [2691, 2704, 5395],
#  "Age": [57.2, 56.7, 56.9],
#  "Female": [1122, 1106, 2228],
#  "Male": [1569, 1598, 3167]}


#
# outcome_list (involves categories)
# outcome_title, measure_title, category_sub_title, group_ids
#

("Incidence of Adjudicated Composite of Symptomatic, Recurrent Venous Thromboembolism (VTE) or VTE-Related Death During 6 Months of Treatment",
 "Number of Participants", None, ["O1", "O2"])
# [2609, 2635]
("Incidence of Adjudicated Composite of Symptomatic, Recurrent Venous Thromboembolism (VTE) or VTE-Related Death During 6 Months of Treatment",
 "Incidence of Adjudicated Composite of Symptomatic, Recurrent Venous Thromboembolism (VTE) or VTE-Related Death During 6 Months of Treatment",
 None, ["O1", "O2"])
# [0.0322, 0.0395]
("Number of Participants With Adverse Events (AEs), Serious AEs (SAEs), Bleeding AEs, Discontinuations Due to AEs and Death During the Treatment Period in Treated Participants",
 "Number of Participants", None, ["O1", "O2"])
# [2676, 2689]
("Number of Participants With Adverse Events (AEs), Serious AEs (SAEs), Bleeding AEs, Discontinuations Due to AEs and Death During the Treatment Period in Treated Participants",
 "Number of Participants With Adverse Events (AEs), Serious AEs (SAEs), Bleeding AEs, Discontinuations Due to AEs and Death During the Treatment Period in Treated Participants",
 "AE", ["O1", "O2"])
# [1795, 1923]
("Number of Participants With Adverse Events (AEs), Serious AEs (SAEs), Bleeding AEs, Discontinuations Due to AEs and Death During the Treatment Period in Treated Participants",
 "Number of Participants With Adverse Events (AEs), Serious AEs (SAEs), Bleeding AEs, Discontinuations Due to AEs and Death During the Treatment Period in Treated Participants",
 "SAE", ["O1", "O2"])
# [417, 410]

# -> simplifying: all that appears common is merging category_sub_titles into a list (only the last two above would change).
# however, for outcomes with no subcats, there would be no natural dict key. DEC: DO NOT SIMPLIFY


#
# serious_events (@subjects_affected only, not @subjects_at_risk - always the same)
# category_type, category_title, event_sub_title, group_ids

('serious', "Cardiac disorders", "MYOCARDIAL INFARCTION", ['E1', 'E2'])
# [2, 3]
('serious', "Cardiac disorders", "CORONARY ARTERY DISEASE", ['E1', 'E2'])
# [1, 5]
('other', "Gastrointestinal disorders", "CONSTIPATION", ['E1', 'E2'])
# [73, 87]

# -> simplifying: same as baseline above. DEC: DO NOT SIMPLIFY
