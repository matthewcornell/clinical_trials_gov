import csv

import click
import mysql.connector


@click.command()
@click.argument('output_file')
@click.argument('nct_id_list', nargs=-1)
def print_adverse_events(output_file, nct_id_list):
    """
    Outputs some example adverse event information for the passed nct_ids, using the MySQL database
    'clinical_trials_gov' as described in readme.md . This is a proof-of-concept for a more general system in the
    future.

    :param: output_file: TSV file to save to
    :param: nct_id_list: list of clinical_study nct_ids to print info for

    Notes:
        - uses Excel tab-delimited format (TSV)
        - indication: a comma-separated list
        - no Dose, Frequency, or Duration information is output at this time (NLP needed) - just '' for now
    """
    conn = mysql.connector.connect(user='root', password='myegan', host='127.0.0.1', database='clinical_trials_gov')
    click.echo("Writing adverse events data to {} for these nct_ids: {}".format(output_file, ', '.join(nct_id_list)))
    with open(output_file, 'w', newline='') as csvfile:
        writer = csv.writer(csvfile, dialect=csv.excel_tab)
        fieldnames = ['NCT', 'Year', 'Phase', 'Indication', 'Test', 'Dose', 'Frequency', 'Duration', 'Comparator',
                      'Dose', 'Frequency', 'Duration', 'Safety Sample', 'Safety Sample', 'All cause Mortality',
                      'All cause Mortality', 'Myocardial Infarction', 'Myocardial Infarction',
                      'Any Severe Adverse Event', 'Any Severe Adverse Event', 'Adverse Event', 'Adverse Event']
        writer.writerow(fieldnames)
        for nct_id in nct_id_list:
            row = []
            # NCT, Year, Phase, Indication,
            completion_year, phase = get_completion_year_phase(conn, nct_id)
            conditions = get_conditions(conn, nct_id)
            row.extend([nct_id, completion_year, phase, ', '.join(conditions)])

            # Test, Dose, Frequency, Duration,
            intervention_type_to_label = get_interventions(conn, nct_id)
            test_intervention = intervention_type_to_label['Experimental']
            row.extend([test_intervention, None, None, None])

            # Comparator, Dose, Frequency, Duration,
            comparator_intervention = intervention_type_to_label['Active Comparator']
            row.extend([comparator_intervention, None, None, None])

            # Safety Sample, Safety Sample,
            label_to_affected_risk = get_reported_events(conn, nct_id, 'Total', 'Total, serious adverse events')
            row.extend([label_to_affected_risk[test_intervention][1],
                        label_to_affected_risk[comparator_intervention][1]])

            # All cause Mortality, All cause Mortality,
            label_to_affected_risk = get_reported_events(conn, nct_id, 'General disorders', 'Death')
            row.extend([label_to_affected_risk[test_intervention][0],
                        label_to_affected_risk[comparator_intervention][0]])

            #   Myocardial Infarction, Myocardial Infarction,
            label_to_affected_risk = get_reported_events(conn, nct_id, 'Cardiac disorders', 'Acute myocardial infarction')
            row.extend([label_to_affected_risk[test_intervention][0],
                        label_to_affected_risk[comparator_intervention][0]])

            #   Any Severe Adverse Event, Any Severe Adverse Event,
            label_to_affected_risk = get_reported_events(conn, nct_id, 'Total', 'Total, serious adverse events')
            row.extend([label_to_affected_risk[test_intervention][0],
                        label_to_affected_risk[comparator_intervention][0]])

            #   Adverse Event, Adverse Event
            label_to_affected_risk = get_reported_events(conn, nct_id, 'Total', 'Total, other adverse events')
            row.extend([label_to_affected_risk[test_intervention][0],
                        label_to_affected_risk[comparator_intervention][0]])

            writer.writerow(row)
    click.echo("Done")


def get_completion_year_phase(conn, nct_id):
    """
    :param conn: connection
    :param nct_id: trial
    :return: 2-tuple of (completion_date, phase) strings for nct_id
    """
    query = """
        SELECT completion_date, phase
        FROM clinical_study
        WHERE nct_id = %s;
    """
    curs = conn.cursor()
    curs.execute(query, (nct_id,))
    rows = curs.fetchone()
    curs.close()

    # format the the results a little
    completion_date, phase = rows[0], rows[1]
    year = completion_date.split()[1] if completion_date else ''
    phase_split = phase.split()
    phase = phase_split[1] if len(phase_split) == 2 else phase
    return year, phase


def get_conditions(conn, nct_id):
    """
    :return: list of conditions as strings for nct_id
    """
    query = """
        SELECT condition_name
        FROM conditions
        WHERE nct_id = %s;
    """
    curs = conn.cursor()
    curs.execute(query, (nct_id,))
    rows = curs.fetchall()
    curs.close()
    return [cond_name_tuple[0] for cond_name_tuple in rows]


def get_interventions(conn, nct_id):
    """
    :param conn: connection
    :param nct_id: trial
    :return: a dict of the form {arm_group_type: arm_group_label, ...} for nct_id, where arm_group_type is either
        'Experimental' or 'Active Comparator'. ex:
            {'Experimental': 'Rivaroxaban (Xarelto, BAY59-7939)', 'Active Comparator': 'Enoxaparin/VKA'}
    """
    query = """
        SELECT ag.arm_group_type, ag.arm_group_label
        FROM ARM_GROUPS ag
        WHERE ag.nct_id = %s
              AND (ag.arm_group_type = 'Experimental' OR ag.arm_group_type = 'Active Comparator');
    """
    curs = conn.cursor()
    curs.execute(query, (nct_id,))
    rows = curs.fetchall()
    curs.close()
    group_type_to_label = {group_type_label_tuple[0]: group_type_label_tuple[1] for group_type_label_tuple in rows}
    return group_type_to_label


def get_reported_events(conn, nct_id, category_title, category_sub_title):
    """
    :return: returns a dict of the form {arm_group_label: (subjects_affected, subjects_at_risk)} for nct_id and the
        passed category title and subtitle. Recall that arm_group_label will be one of the values of
        get_interventions(). ex:
            {'Enoxaparin/VKA': ('252', '1711'), 'Rivaroxaban (Xarelto, BAY59-7939)': ('222', '1718')}
    """
    query = """
        SELECT ag.arm_group_label, recg.subjects_affected, recg.subjects_at_risk
        FROM reported_event_ctgy rec
          JOIN reported_events re ON rec.reported_event_id = re.reported_event_id
          JOIN reported_event_ctgy_grp recg ON rec.reported_event_category_id = recg.reported_event_category_id
          JOIN arm_groups ag ON recg.arm_group_id = ag.arm_group_id
        WHERE re.NCT_ID = %s AND rec.category_title = %s AND rec.category_sub_title = %s
        ORDER BY ag.arm_group_label;
    """
    curs = conn.cursor()
    curs.execute(query, (nct_id, category_title, category_sub_title))
    rows = curs.fetchall()
    curs.close()
    label_to_affected_risk = {group_type_label_tuple[0]: (group_type_label_tuple[1], group_type_label_tuple[2])
                              for group_type_label_tuple in rows}
    return label_to_affected_risk


if __name__ == '__main__':
    print_adverse_events()
