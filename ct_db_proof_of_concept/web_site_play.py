import click
import mysql.connector

from ct_db_proof_of_concept.print_adverse_events_proof_of_concept import get_interventions


@click.command()
def web_site_play():
    conn = mysql.connector.connect(user='root', password='myegan', host='127.0.0.1', database='clinical_trials_gov')

    # * 1) Enter NCT_ID: NCT00440193
    # -> user enters:
    nct_id = 'NCT00440193'
    print("nct_id={}".format(nct_id))

    # 2) prep: retrieve Reporting Groups. Q: ag.arm_group_id?
    intervention_type_to_label = get_interventions(conn, nct_id)
    # {'Experimental': 'Rivaroxaban (Xarelto, BAY59-7939)', 'Active Comparator': 'Enoxaparin/VKA'}

    # * 2) Participant Flow > Reporting Groups: -> user checks both:
    user_arm_groups = list(intervention_type_to_label.values())
    print("user_arm_groups={}".format(user_arm_groups))
    # ['Rivaroxaban (Xarelto, BAY59-7939)', 'Enoxaparin/VKA']

    # 3) prep: retrieve Periods
    periods = get_periods(conn, nct_id)

    # * 3) Participant Flow > Periods: -> user checks both:
    user_periods = periods
    print("user_periods={}".format(user_periods))
    # [(5573, 'Treatment Period'), (5800, 'Observational Period')]

    # 4a) prep: retrieve milestone_id info
    period_to_id_type_title = {period[0]: get_milestone_ids_for_period(conn, nct_id, period[0])
                               for period in user_periods}

    # * 4a) Participant Flow > Period 1: Treatment Period > Started/Completed/Not Completed:
    # * 4b) Participant Flow > Period 2: Observational Period > Started/Completed/Not Completed:
    # -> user checks:
    users_periods_to_id_type_title = {  # probably all we need is {period: ids}
        5573: [(14303, True, 'Participants Received Treatment'),
               (14304, True, 'COMPLETED'),
               (14305, True, 'NOT COMPLETED'),
               (16726, True, 'STARTED'),
               (57405, False, 'Adverse Event'),
               # (57406, False, 'Death'),
               # (57407, False, 'Lack of Efficacy'),
               # (57408, False, 'Lost to Follow-up'),
               # (57409, False, 'Physician Decision'),
               # (57410, False, 'Protocol Violation'),
               # (57411, False, 'Withdrawal by Subject'),
               # (57412, False, 'Clinical endpoint reached'),
               # (57413, False, 'Study terminated by sponsor'),
               # (57414, False, 'Protocol driven decision point'),
               # (57415, False, 'Switch to commercial drug'),
               # (57416, False, 'Technical problems'),
               # (57417, False, 'Participant convenience'),
               # (57418, False, 'Participant did not receive treatment'),
               (57419, False, 'Site closed by investigator')],
        5800: [(14306, True, 'STARTED'),
               (14307, True, 'COMPLETED'),
               (14308, True, 'NOT COMPLETED'),
               (57420, False, 'Adverse Event'),
               # (57421, False, 'Death'),
               # (58029, False, 'Lost to Follow-up'),
               # (58030, False, 'Protocol Violation'),
               # (58031, False, 'Withdrawal by Subject'),
               # (58032, False, 'Physician Decision'),
               # (58033, False, 'Clinical endpoint reached'),
               (58034, False, 'Participant convenience')],
    }
    print("users_periods_to_id_type_title={}".format(users_periods_to_id_type_title))

    # 5) prep: retrieve outcome_id info
    outcome_ids = get_outcome_ids(conn, nct_id)

    # * 5) Outcome Measures: -> user checks:
    user_outcome_ids = [  # probably all we need is [ids]
        (31794, 'Other Pre-specified', 'Percentage of Participants With the Individual Components of Efficacy ...'),
        # (31795, 'Other Pre-specified', 'Percentage of Participants With Symptomatic Recurrent Venous Thromboem...'),
        # (31796, 'Other Pre-specified', 'Percentage of Participants With the Composite Variable Comprising Recu...'),
        # (31797, 'Other Pre-specified', 'Percentage of Participants With an Event for Net Clinical Benefit 1 Du...'),
        # (31951, 'Other Pre-specified', 'Percentage of Participants With Recurrent DVT During Observational Per...'),
        # (31952, 'Other Pre-specified', 'Percentage of Participants With the Individual Components of Efficacy ...'),
        # (31953, 'Post-Hoc', 'Percentage of Participants With an Event for Net Clinical Benefit 2 Until the Int...'),
        # (31954, 'Post-Hoc', 'Percentage of Participants With an Event for Net Clinical Benefit 2 During Observ...'),
        # (32221, 'Secondary', 'Percentage of Participants With an Event for Net Clinical Benefit 1 Until the In...'),
        # (32222, 'Secondary', 'Percentage of Participants With Recurrent DVT Until the Intended End of Study Tr...'),
        # (32223, 'Secondary', 'Percentage of Participants With Clinically Relevant Bleeding, Treatment-emergent...'),
        # (32325, 'Secondary', 'Percentage of Participants With All Deaths'),
        # (32326, 'Secondary', 'Percentage of Participants With Other Vascular Events, On-treatment (Time Window...'),
        (32786, 'Primary', 'Percentage of Participants With Symptomatic Recurrent Venous Thromboembolism [VTE]...'),
        # (32787, 'Secondary', 'Percentage of Participants With the Composite Variable Comprising Recurrent DVT,...'),
    ]
    print("user_outcome_ids={}".format(user_outcome_ids))

    # * 6a) Outcome Measures > Other Outcome Measures > 1. Primary: Percentage of Participants With Symptomatic ...
    # todo

    # * 6b) Outcome Measures > Other Outcome Measures > 8. Other Pre-specified: Percentage of Participants With the ...
    # todo

    # * 7) Serious Adverse Events:
    # todo

    # * 8) Other Adverse Events:
    # todo

    # * 9) Done: [Submit button]
    # todo


def get_outcome_ids(conn, nct_id):
    """
    :return: list of 3-tuples: (outcome_id, outcome_type, outcome_title)
    """
    query = """
        SELECT ro.OUTCOME_ID, ro.OUTCOME_TYPE, ro.OUTCOME_TITLE
        FROM RESULTS_OUTCOMES ro
        WHERE ro.nct_id = %s;
    """
    curs = conn.cursor()
    curs.execute(query, (nct_id,))
    rows = curs.fetchall()
    curs.close()
    return rows


def get_milestone_ids_for_period(conn, nct_id, partic_flow_id):
    """
    :param partic_flow_id:
    :return: list of 3-tuples: (milestone_id, is_milestone, milestone_title). is_milestone is True if
        milestone_title = 'Milestone', and False if it = 'Drop Withdraw Reason'
    """
    query = """
        SELECT rpm.MILESTONE_ID, rpm.MILESTONE_TYPE, rpm.MILESTONE_TITLE
        FROM RESULTS_PARTIC_FLOWS rpf
          JOIN RESULTS_PARTFLOW_MLSTN rpm ON rpf.PARTICIPANT_FLOW_ID = rpm.PARTICIPANT_FLOW_ID
        WHERE rpf.nct_id = %s
              AND rpf.PARTICIPANT_FLOW_ID = %s
    """
    curs = conn.cursor()
    curs.execute(query, (nct_id, partic_flow_id))
    rows = curs.fetchall()
    curs.close()
    return [(row[0], row[1] == 'Milestone', row[2]) for row in rows]


def get_periods(conn, nct_id):
    """
    :return: a list of 2-tuples of participant flow periods in nct_id. format: (participant_flow_id, period_title)
    """
    query = """
        SELECT rpf.PARTICIPANT_FLOW_ID, rpf.PERIOD_TITLE
        FROM RESULTS_PARTIC_FLOWS rpf
        WHERE rpf.nct_id = %s;
    """
    curs = conn.cursor()
    curs.execute(query, (nct_id,))
    rows = curs.fetchall()
    curs.close()
    return rows


if __name__ == '__main__':
    web_site_play()
