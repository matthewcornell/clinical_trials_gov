LOAD DATA LOCAL INFILE '/Users/cornellm/Downloads/AACT201509_pipe_delimited_txt/clinical_study.txt' INTO TABLE clinical_trials_gov.clinical_study
    FIELDS TERMINATED BY '|' LINES TERMINATED BY '\r\n' IGNORE 1 LINES;
LOAD DATA LOCAL INFILE '/Users/cornellm/Downloads/AACT201509_pipe_delimited_txt/arm_groups.txt' INTO TABLE clinical_trials_gov.arm_groups
    FIELDS TERMINATED BY '|' LINES TERMINATED BY '\r\n' IGNORE 1 LINES;
LOAD DATA LOCAL INFILE '/Users/cornellm/Downloads/AACT201509_pipe_delimited_txt/authorities.txt' INTO TABLE clinical_trials_gov.authorities
    FIELDS TERMINATED BY '|' LINES TERMINATED BY '\r\n' IGNORE 1 LINES;
LOAD DATA LOCAL INFILE '/Users/cornellm/Downloads/AACT201509_pipe_delimited_txt/central_contacts.txt' INTO TABLE clinical_trials_gov.central_contacts
    FIELDS TERMINATED BY '|' LINES TERMINATED BY '\r\n' IGNORE 1 LINES;
LOAD DATA LOCAL INFILE '/Users/cornellm/Downloads/AACT201509_pipe_delimited_txt/condition_browse.txt' INTO TABLE clinical_trials_gov.condition_browse
    FIELDS TERMINATED BY '|' LINES TERMINATED BY '\r\n' IGNORE 1 LINES;
LOAD DATA LOCAL INFILE '/Users/cornellm/Downloads/AACT201509_pipe_delimited_txt/conditions.txt' INTO TABLE clinical_trials_gov.conditions
    FIELDS TERMINATED BY '|' LINES TERMINATED BY '\r\n' IGNORE 1 LINES;
LOAD DATA LOCAL INFILE '/Users/cornellm/Downloads/AACT201509_pipe_delimited_txt/designs.txt' INTO TABLE clinical_trials_gov.designs
    FIELDS TERMINATED BY '|' LINES TERMINATED BY '\r\n' IGNORE 1 LINES;
LOAD DATA LOCAL INFILE '/Users/cornellm/Downloads/AACT201509_pipe_delimited_txt/facilities.txt' INTO TABLE clinical_trials_gov.facilities
    FIELDS TERMINATED BY '|' LINES TERMINATED BY '\r\n' IGNORE 1 LINES;
LOAD DATA LOCAL INFILE '/Users/cornellm/Downloads/AACT201509_pipe_delimited_txt/facility_contacts.txt' INTO TABLE clinical_trials_gov.facility_contacts
    FIELDS TERMINATED BY '|' LINES TERMINATED BY '\r\n' IGNORE 1 LINES;
LOAD DATA LOCAL INFILE '/Users/cornellm/Downloads/AACT201509_pipe_delimited_txt/intervention_arm_groups.txt' INTO TABLE clinical_trials_gov.intervention_arm_groups
    FIELDS TERMINATED BY '|' LINES TERMINATED BY '\r\n' IGNORE 1 LINES;
LOAD DATA LOCAL INFILE '/Users/cornellm/Downloads/AACT201509_pipe_delimited_txt/intervention_browse.txt' INTO TABLE clinical_trials_gov.intervention_browse
    FIELDS TERMINATED BY '|' LINES TERMINATED BY '\r\n' IGNORE 1 LINES;
LOAD DATA LOCAL INFILE '/Users/cornellm/Downloads/AACT201509_pipe_delimited_txt/intervention_other_names.txt' INTO TABLE clinical_trials_gov.intervention_other_names
    FIELDS TERMINATED BY '|' LINES TERMINATED BY '\r\n' IGNORE 1 LINES;
LOAD DATA LOCAL INFILE '/Users/cornellm/Downloads/AACT201509_pipe_delimited_txt/interventions.txt' INTO TABLE clinical_trials_gov.interventions
    FIELDS TERMINATED BY '|' LINES TERMINATED BY '\r\n' IGNORE 1 LINES;
LOAD DATA LOCAL INFILE '/Users/cornellm/Downloads/AACT201509_pipe_delimited_txt/investigators.txt' INTO TABLE clinical_trials_gov.investigators
    FIELDS TERMINATED BY '|' LINES TERMINATED BY '\r\n' IGNORE 1 LINES;
LOAD DATA LOCAL INFILE '/Users/cornellm/Downloads/AACT201509_pipe_delimited_txt/keywords.txt' INTO TABLE clinical_trials_gov.keywords
    FIELDS TERMINATED BY '|' LINES TERMINATED BY '\r\n' IGNORE 1 LINES;
LOAD DATA LOCAL INFILE '/Users/cornellm/Downloads/AACT201509_pipe_delimited_txt/links.txt' INTO TABLE clinical_trials_gov.links
    FIELDS TERMINATED BY '|' LINES TERMINATED BY '\r\n' IGNORE 1 LINES;
LOAD DATA LOCAL INFILE '/Users/cornellm/Downloads/AACT201509_pipe_delimited_txt/location_countries.txt' INTO TABLE clinical_trials_gov.location_countries
    FIELDS TERMINATED BY '|' LINES TERMINATED BY '\r\n' IGNORE 1 LINES;
LOAD DATA LOCAL INFILE '/Users/cornellm/Downloads/AACT201509_pipe_delimited_txt/mesh_thesaurus.txt' INTO TABLE clinical_trials_gov.mesh_thesaurus
    FIELDS TERMINATED BY '|' LINES TERMINATED BY '\r\n' IGNORE 1 LINES;
LOAD DATA LOCAL INFILE '/Users/cornellm/Downloads/AACT201509_pipe_delimited_txt/nct_aliases.txt' INTO TABLE clinical_trials_gov.nct_aliases
    FIELDS TERMINATED BY '|' LINES TERMINATED BY '\r\n' IGNORE 1 LINES;
LOAD DATA LOCAL INFILE '/Users/cornellm/Downloads/AACT201509_pipe_delimited_txt/overall_officials.txt' INTO TABLE clinical_trials_gov.overall_officials
    FIELDS TERMINATED BY '|' LINES TERMINATED BY '\r\n' IGNORE 1 LINES;
LOAD DATA LOCAL INFILE '/Users/cornellm/Downloads/AACT201509_pipe_delimited_txt/references.txt' INTO TABLE clinical_trials_gov.reference
    FIELDS TERMINATED BY '|' LINES TERMINATED BY '\r\n' IGNORE 1 LINES;
LOAD DATA LOCAL INFILE '/Users/cornellm/Downloads/AACT201509_pipe_delimited_txt/removed_countries.txt' INTO TABLE clinical_trials_gov.removed_countries
    FIELDS TERMINATED BY '|' LINES TERMINATED BY '\r\n' IGNORE 1 LINES;
LOAD DATA LOCAL INFILE '/Users/cornellm/Downloads/AACT201509_pipe_delimited_txt/reported_event_ctgy.txt' INTO TABLE clinical_trials_gov.reported_event_ctgy
    FIELDS TERMINATED BY '|' LINES TERMINATED BY '\r\n' IGNORE 1 LINES;
LOAD DATA LOCAL INFILE '/Users/cornellm/Downloads/AACT201509_pipe_delimited_txt/reported_event_ctgy_grp.txt' INTO TABLE clinical_trials_gov.reported_event_ctgy_grp
    FIELDS TERMINATED BY '|' LINES TERMINATED BY '\r\n' IGNORE 1 LINES;
LOAD DATA LOCAL INFILE '/Users/cornellm/Downloads/AACT201509_pipe_delimited_txt/reported_events.txt' INTO TABLE clinical_trials_gov.reported_events
    FIELDS TERMINATED BY '|' LINES TERMINATED BY '\r\n' IGNORE 1 LINES;
LOAD DATA LOCAL INFILE '/Users/cornellm/Downloads/AACT201509_pipe_delimited_txt/responsible_parties.txt' INTO TABLE clinical_trials_gov.responsible_parties
    FIELDS TERMINATED BY '|' LINES TERMINATED BY '\r\n' IGNORE 1 LINES;
LOAD DATA LOCAL INFILE '/Users/cornellm/Downloads/AACT201509_pipe_delimited_txt/results_baseline.txt' INTO TABLE clinical_trials_gov.results_baseline
    FIELDS TERMINATED BY '|' LINES TERMINATED BY '\r\n' IGNORE 1 LINES;
LOAD DATA LOCAL INFILE '/Users/cornellm/Downloads/AACT201509_pipe_delimited_txt/results_baseline_measure.txt' INTO TABLE clinical_trials_gov.results_baseline_measures
    FIELDS TERMINATED BY '|' LINES TERMINATED BY '\r\n' IGNORE 1 LINES;
LOAD DATA LOCAL INFILE '/Users/cornellm/Downloads/AACT201509_pipe_delimited_txt/results_baseline_measure_catgy.txt' INTO TABLE clinical_trials_gov.results_baseline_measure_catgy
    FIELDS TERMINATED BY '|' LINES TERMINATED BY '\r\n' IGNORE 1 LINES;
LOAD DATA LOCAL INFILE '/Users/cornellm/Downloads/AACT201509_pipe_delimited_txt/results_outcome_analysis.txt' INTO TABLE clinical_trials_gov.results_outcome_analysis
    FIELDS TERMINATED BY '|' LINES TERMINATED BY '\r\n' IGNORE 1 LINES;
LOAD DATA LOCAL INFILE '/Users/cornellm/Downloads/AACT201509_pipe_delimited_txt/results_outcome_analysis_grp.txt' INTO TABLE clinical_trials_gov.results_outcome_analysis_grp
    FIELDS TERMINATED BY '|' LINES TERMINATED BY '\r\n' IGNORE 1 LINES;
LOAD DATA LOCAL INFILE '/Users/cornellm/Downloads/AACT201509_pipe_delimited_txt/results_outcome_measure.txt' INTO TABLE clinical_trials_gov.results_outcome_measure
    FIELDS TERMINATED BY '|' LINES TERMINATED BY '\r\n' IGNORE 1 LINES;
LOAD DATA LOCAL INFILE '/Users/cornellm/Downloads/AACT201509_pipe_delimited_txt/results_outcome_measure_ctgy.txt' INTO TABLE clinical_trials_gov.results_outcome_measure_ctgy
    FIELDS TERMINATED BY '|' LINES TERMINATED BY '\r\n' IGNORE 1 LINES;
LOAD DATA LOCAL INFILE '/Users/cornellm/Downloads/AACT201509_pipe_delimited_txt/results_outcomes.txt' INTO TABLE clinical_trials_gov.results_outcomes
    FIELDS TERMINATED BY '|' LINES TERMINATED BY '\r\n' IGNORE 1 LINES;
LOAD DATA LOCAL INFILE '/Users/cornellm/Downloads/AACT201509_pipe_delimited_txt/results_partflow_mlstn.txt' INTO TABLE clinical_trials_gov.results_partflow_mlstn
    FIELDS TERMINATED BY '|' LINES TERMINATED BY '\r\n' IGNORE 1 LINES;
LOAD DATA LOCAL INFILE '/Users/cornellm/Downloads/AACT201509_pipe_delimited_txt/results_partflow_mlstn_grp.txt' INTO TABLE clinical_trials_gov.results_partflow_mlstn_grp
    FIELDS TERMINATED BY '|' LINES TERMINATED BY '\r\n' IGNORE 1 LINES;
LOAD DATA LOCAL INFILE '/Users/cornellm/Downloads/AACT201509_pipe_delimited_txt/results_partic_flows.txt' INTO TABLE clinical_trials_gov.results_partic_flows
    FIELDS TERMINATED BY '|' LINES TERMINATED BY '\r\n' IGNORE 1 LINES;
LOAD DATA LOCAL INFILE '/Users/cornellm/Downloads/AACT201509_pipe_delimited_txt/results_point_of_contact.txt' INTO TABLE clinical_trials_gov.results_point_of_contact
    FIELDS TERMINATED BY '|' LINES TERMINATED BY '\r\n' IGNORE 1 LINES;
LOAD DATA LOCAL INFILE '/Users/cornellm/Downloads/AACT201509_pipe_delimited_txt/results_restriction_agreement.txt' INTO TABLE clinical_trials_gov.results_restriction_agreements
    FIELDS TERMINATED BY '|' LINES TERMINATED BY '\r\n' IGNORE 1 LINES;
LOAD DATA LOCAL INFILE '/Users/cornellm/Downloads/AACT201509_pipe_delimited_txt/secondary_ids.txt' INTO TABLE clinical_trials_gov.secondary_ids
    FIELDS TERMINATED BY '|' LINES TERMINATED BY '\r\n' IGNORE 1 LINES;
LOAD DATA LOCAL INFILE '/Users/cornellm/Downloads/AACT201509_pipe_delimited_txt/sponsors.txt' INTO TABLE clinical_trials_gov.sponsors
    FIELDS TERMINATED BY '|' LINES TERMINATED BY '\r\n' IGNORE 1 LINES;
LOAD DATA LOCAL INFILE '/Users/cornellm/Downloads/AACT201509_pipe_delimited_txt/study_outcome.txt' INTO TABLE clinical_trials_gov.study_outcome
    FIELDS TERMINATED BY '|' LINES TERMINATED BY '\r\n' IGNORE 1 LINES;
